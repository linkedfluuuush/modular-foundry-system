import { ModularActorSheet } from './actor-sheet.mjs';
import templateFunctions from './template-functions.mjs';

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class TemplateSheet extends ModularActorSheet {
    /* -------------------------------------------- */

    /** @override */
    async getData() {
        // Retrieve the data structure from the base sheet. You can inspect or log
        // the context variable to see the structure, but some key properties for
        // sheets are the actor object, the data object, whether or not it's
        // editable, the items array, and the effects array.
        const context = super.getData();

        // Prepare character data and items.
        await this._prepareSheetData(context);

        return context;
    }

    /**
     * Organize and classify Items for Character sheets.
     *
     * @param {Object} actorData The actor to prepare.
     *
     * @return {undefined}
     */
    async _prepareSheetData(context) {
        await super._preRenderSheet(context, true);
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // -------------------------------------------------------------
        // Everything below here is only needed if the sheet is editable
        if (!this.isEditable) return;

        // Add tab button
        html.find('.mfs-add-template-tab').click((ev) => {
            // Create dialog for tab edition
            templateFunctions.editTab((name, key) => {
                // This is called on dialog validation

                // Checking for duplicate keys
                let existingTab = this.actor.data.data.tabs.filter(
                    (tab) => tab.key === key
                );

                if (existingTab.length > 0) {
                    ui.notifications.error(
                        'Could not create tab with duplicate key ' + key
                    );
                } else {
                    // Adding the new tab to the template
                    this.actor.data.data.tabs.push({
                        name,
                        key,
                        contents: []
                    });

                    this.actor
                        .update({
                            data: {
                                hidden: this.actor.data.data.hidden,
                                header: this.actor.data.data.header,
                                tabs: this.actor.data.data.tabs
                            }
                        })
                        .then(() => {
                            console.debug(
                                'Added tab ' + name + ' (' + key + ')'
                            );
                            this.render(false);
                        });
                }
            });
        });

        // Edit tab button
        html.find('.mfs-edit-template-tab').click((ev) => {
            // Fetch tab data for edition
            let tabKey = $('.tabs .active').parents('.mfs-template-tab-name');
            let data = {
                key: String(tabKey.data('tab-key')),
                name: String(tabKey.data('tab-name'))
            };

            // Create dialog for tab edition
            // Tab data is added as second argument
            templateFunctions.editTab((name, key) => {
                // This is called on dialog validation

                // Checking for duplicate keys
                let existingTab = this.actor.data.data.tabs.filter(
                    (tab) => tab.key === key
                );

                if (existingTab.length > 0 && key !== data.key) {
                    ui.notifications.error(
                        'Could not edit tab with duplicate key ' + key
                    );
                } else {
                    // Updating tab data
                    let objIndex = this.actor.data.data.tabs.findIndex(
                        (tab) => tab.key === data.key
                    );

                    this.actor.data.data.tabs[objIndex].name = name;
                    this.actor.data.data.tabs[objIndex].key = key;

                    this.actor
                        .update({
                            data: {
                                hidden: this.actor.data.data.hidden,
                                header: this.actor.data.data.header,
                                tabs: this.actor.data.data.tabs
                            }
                        })
                        .then(() => {
                            console.debug(
                                'Edited tab ' + name + ' (' + key + ')'
                            );
                            this.render(false);
                        });
                }
            }, data);
        });

        // Delete tab button
        html.find('.mfs-delete-template-tab').click((ev) => {
            // Fetching tab key
            let tabKey = $('.tabs .active').parents('.mfs-template-tab-name');
            let key = String(tabKey.data('tab-key'));

            // Deletion is made by filtering tab array
            this.actor.data.data.tabs = this.actor.data.data.tabs.filter(
                (tab) => tab.key !== key
            );

            this.actor
                .update({
                    data: {
                        hidden: this.actor.data.data.hidden,
                        header: this.actor.data.data.header,
                        tabs: this.actor.data.data.tabs
                    }
                })
                .then(() => {
                    console.debug('Removed tab ' + key);
                    this.render(false);
                });
        });

        // Move tab to the right button
        html.find('.mfs-sort-right-template-tab').click((ev) => {
            // Fetching tab key
            let tabKey = $(ev.currentTarget).parents('.mfs-template-tab-name');
            let key = tabKey.data('tab-key');

            // Getting tab index in tab array
            let objIndex = this.actor.data.data.tabs.findIndex(
                (tab) => tab.key === key
            );

            // Moving tab one place down the array, if it is not already the last tab
            if (objIndex !== this.actor.data.data.tabs.length - 1) {
                let tmp = this.actor.data.data.tabs[objIndex];
                this.actor.data.data.tabs[objIndex] =
                    this.actor.data.data.tabs[objIndex + 1];
                this.actor.data.data.tabs[objIndex + 1] = tmp;
            }

            this.actor
                .update({
                    data: {
                        hidden: this.actor.data.data.hidden,
                        header: this.actor.data.data.header,
                        tabs: this.actor.data.data.tabs
                    }
                })
                .then(() => {
                    console.debug('Sort tab ' + key + ' to the right');
                    this.render(false);
                });
        });

        // Move tab to the left button
        html.find('.mfs-sort-left-template-tab').click((ev) => {
            // Fetching tab key
            let tabKey = $(ev.currentTarget).parents('.mfs-template-tab-name');
            let key = tabKey.data('tab-key');

            // Getting tab index in tab array
            let objIndex = this.actor.data.data.tabs.findIndex(
                (tab) => tab.key === key
            );

            // Moving tab one place down the array, if it is not already the first tab
            if (objIndex !== 0) {
                let tmp = this.actor.data.data.tabs[objIndex];
                this.actor.data.data.tabs[objIndex] =
                    this.actor.data.data.tabs[objIndex - 1];
                this.actor.data.data.tabs[objIndex - 1] = tmp;
            }

            this.actor
                .update({
                    data: {
                        hidden: this.actor.data.data.hidden,
                        header: this.actor.data.data.header,
                        tabs: this.actor.data.data.tabs
                    }
                })
                .then(() => {
                    console.debug('Sort tab ' + key + ' to the left');
                    this.render(false);
                });
        });

        // Add a new component
        html.find('.mfs-template-tab-controls .mfs-add-component').click(
            (ev) => {
                // Fetch new component's container path
                let target = $(ev.currentTarget);
                let newComponentPath = target.data('path');

                // Open dialog to edit new component
                templateFunctions.componentDialog((action, component) => {
                    // This is called on dialog validation

                    // Add component
                    this._addTemplateItem(newComponentPath, component);

                    this.actor
                        .update({
                            data: {
                                hidden: this.actor.data.data.hidden,
                                header: this.actor.data.data.header,
                                tabs: this.actor.data.data.tabs
                            }
                        })
                        .then(() => {
                            this.render(false);
                        });
                });
            }
        );

        // Add column in dynamic table
        // This is done differently than other components because there are specific properties to dynamic table columns
        html.find('.mfs-add-dynamic-table-component').click((ev) => {
            let target = $(ev.currentTarget);
            let newComponentPath = target.data('path');

            // Open dialog to edit new column
            templateFunctions.componentDialog(
                (action, component) => {
                    // This is called on dialog validation

                    // Adding column
                    this._addDynamicTableItem(newComponentPath, component);

                    this.actor
                        .update({
                            data: {
                                hidden: this.actor.data.data.hidden,
                                header: this.actor.data.data.header,
                                tabs: this.actor.data.data.tabs
                            }
                        })
                        .then(() => {
                            this.render(false);
                        });
                },
                { isDynamic: true }
            );
        });

        // Edit component
        html.find('.mfs-editable-component').click((ev) => {
            // Get component's path
            let target = $(ev.currentTarget);
            let templatePath = target.data('path');

            // Get component definition in template
            let part = this.actor.data.data;
            let splitPath = templatePath.split('.');

            for (let pathPart of splitPath) {
                part = part[pathPart];
            }

            // If the path contains rowLayout, we are editing a Dynamic Table's column
            if (templatePath.includes('rowLayout')) {
                part.isDynamic = true;
            }

            // Open dialog to edit component
            templateFunctions.componentDialog((action, component) => {
                // This is called on dialog validation

                // Dialog has many buttons, clicked button is returned in action
                // New component data is returns in component

                // If we edit the component
                if (action === 'edit') {
                    // Get back the part from template definition
                    let part = this.actor.data.data;
                    let splitPath = templatePath.split('.');

                    for (let pathPart of splitPath) {
                        part = part[pathPart];
                    }

                    // If new component and existing component don't have the same type, we wipe the existing component
                    if (component.type !== part.type) {
                        Object.keys(part).forEach(function (key) {
                            delete part[key];
                        });
                    }

                    // We set the new component into the existing one
                    // It is easier there to work by reference than by assigning the new component directly
                    Object.keys(component).forEach(function (key) {
                        part[key] = component[key];
                    });
                } else if (action === 'delete') {
                    // Get the component
                    let part = this.actor.data.data;
                    let parent;
                    let parentType = 'tab';
                    let splitPath = templatePath.split('.');

                    for (let pathPart of splitPath) {
                        parent = part;
                        if (parent.type) {
                            parentType = parent.type;
                        }
                        part = part[pathPart];
                    }

                    // The last part of splitPath is the path relative to the components direct parent
                    // So it is set to null to be deleted
                    parent[splitPath.pop()] = null;

                    // If not a table, we remove every null in contents
                    if (parentType !== 'table') {
                        let len = parent.length;
                        for (let i = 0; i < len; i++) {
                            parent[i] && parent.push(parent[i]); // copy non-empty values to the end of the array
                        }

                        parent.splice(0, len);
                    }
                } else if (action === 'sortUp') {
                    // Sort before in the same container

                    // Get part
                    let part = this.actor.data.data;
                    let parent;
                    let parentType = 'tab';
                    let splitPath = templatePath.split('.');

                    for (let pathPart of splitPath) {
                        parent = part;
                        if (parent.type) {
                            parentType = parent.type;
                        }
                        part = part[pathPart];
                    }

                    let idx = parseInt(splitPath.pop());

                    // Sort is not possible in tables
                    if (parentType !== 'table' && idx > 0) {
                        parent[idx] = parent[idx - 1];
                        parent[idx - 1] = part;
                    }
                } else if (action === 'sortDown') {
                    // Sort after in the same container

                    // Get part
                    let part = this.actor.data.data;
                    let parent;
                    let parentType = 'tab';
                    let splitPath = templatePath.split('.');

                    for (let pathPart of splitPath) {
                        parent = part;
                        if (parent.type) {
                            parentType = parent.type;
                        }
                        part = part[pathPart];
                    }

                    let idx = parseInt(splitPath.pop());

                    // Sort is not possible in tables
                    if (parentType !== 'table' && idx < parent.length - 1) {
                        parent[idx] = parent[idx + 1];
                        parent[idx + 1] = part;
                    }
                }

                // After actions have been taken care of, save actor
                this.actor
                    .update({
                        data: {
                            hidden: this.actor.data.data.hidden,
                            header: this.actor.data.data.header,
                            tabs: this.actor.data.data.tabs
                        }
                    })
                    .then(() => {
                        this.render(false);
                    });
            }, part);
        });

        // Edit hidden attributes
        html.find('.mfs-configure-attributes').click((ev) => {
            // Open the dialog for edition
            templateFunctions.attributesDialog((newAttributes) => {
                // This is called on dialog validation

                // Update the actor with new hidden attributes
                this.actor
                    .update({
                        data: {
                            hidden: newAttributes
                        }
                    })
                    .then(() => {
                        this.render(false);
                    });
            }, this.actor.data.data.hidden);
        });
    }

    /**
     * Adds a new item to the template, at the specified path
     * @param path
     * @param item
     * @private
     */
    _addTemplateItem(path, item) {
        // Get component a specified path
        let part = this.actor.data.data;

        let splitPath = path.split('.');

        // If path has table coordinates at the end, we are adding a component to a table
        let tablePos = path.match(/\[[0-9]+,[0-9]+]$/);

        if (tablePos) {
            // In that case, we calculate the tables coordinates, and remove the coordinates from the path
            tablePos = tablePos[0].substr(1).slice(0, -1).split(',');
            splitPath.pop();
        }

        // Get the parent
        for (let pathPart of splitPath) {
            part = part[pathPart];
        }

        // If the container does not have contents yet, we initialize an empty array
        if (!part.contents) {
            part.contents = [];
        }

        if (tablePos) {
            // If it's a table, we must ensure the second array exists as well
            // All indices do not need to exist in javascript tables, so we initialize only the ones we need to add components in
            if (!part.contents[tablePos[0]]) {
                part.contents[tablePos[0]] = [];
            }
            part.contents[tablePos[0]][tablePos[1]] = item;
        } else {
            // If it's a regular component, we just add the item
            part.contents.push(item);
        }
    }

    /**
     * Adds a column to a Dynamic Table
     * @param path
     * @param item
     * @private
     */
    _addDynamicTableItem(path, item) {
        // Get the dynamic table
        let part = this.actor.data.data;

        let splitPath = path.split('.');

        for (let pathPart of splitPath) {
            part = part[pathPart];
        }

        // Dynamic table store their "components" inside rowLayout
        // We initialize it if it does not exist yet
        if (!part.rowLayout) {
            part.rowLayout = [];
        }

        // ... and add the new column
        part.rowLayout.push(item);
    }
}
