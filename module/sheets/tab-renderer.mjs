// TODO make every component into a Class

import ComputablePhrase from '../formulas/computablePhrase.mjs';

export const renderTab = async (contents, props, isTemplate, path) => {
    let render = '<div class="flexcol flex-group-center">';

    render += await renderContents(contents, props, isTemplate, path);

    if (isTemplate) {
        render += `<div class="mfs-template-tab-controls">`;

        render += `<a class="item" title="Add new element"><i
                    class="fas fa-plus-circle mfs-clickable mfs-add-component" data-path="${path}"></i></a>`;

        render += `</div>`;
    }

    render += '</div>';

    return new Handlebars.SafeString(render);
};

const renderContents = async (contents, props, isTemplate, path) => {
    let render = '';
    if (Array.isArray(contents)) {
        for(let idx of contents.keys()){
            let component = contents[idx];

            render += await renderComponent(
                component,
                props,
                isTemplate,
                path + '.contents.' + idx
            );
        }
    }

    return render;
};

const renderComponent = async (component, props, isTemplate, path) => {
    let render;

    switch (component.type) {
        case 'panel':
            render = await renderPanel(component, props, isTemplate, path);
            break;
        case 'label':
            render = await renderLabel(component, props, isTemplate, path);
            break;
        case 'table':
            render = await renderTable(component, props, isTemplate, path);
            break;
        case 'dynamicTable':
            render = await renderDynamicTable(component, props, isTemplate, path);
            break;
        case 'checkbox':
            render = renderCheckbox(component, props, isTemplate, path);
            break;
        case 'textField':
            render = renderTextField(component, props, isTemplate, path);
            break;
        case 'textArea':
            render = renderTextArea(component, props, isTemplate, path);
            break;
        case 'select':
            render = renderSelect(component, props, isTemplate, path);
            break;
        default:
            render = 'INVALID COMPONENT TYPE ' + component.type;
            break;
    }

    return render;
};

const renderPanel = async (panelConf, props, isTemplate, path) => {
    let keyClass = panelConf.key ?? '';

    let layoutClass;

    switch (panelConf.flow) {
        case 'vertical':
            layoutClass = '';
            break;
        case 'horizontal':
            layoutClass = 'flexrow';
            break;
        default:
            if (/^grid-([1-9]$|1[0-2]$)/.test(panelConf.flow)) {
                layoutClass = 'grid grid-' + panelConf.flow.substr(5) + 'col';
            } else {
                layoutClass = '';
            }
            break;
    }

    let alignClass;
    switch (panelConf.align) {
        case 'center':
            alignClass = 'flex-group-center';
            break;
        case 'left':
            alignClass = 'flex-group-left';
            break;
        case 'right':
            alignClass = 'flex-group-right';
            break;
        case 'justify':
            alignClass = 'flex-between';
            break;
        default:
            alignClass = '';
            break;
    }

    let render = '';
    if (isTemplate) {
        render += `<div class="mfs-editable-panel"><div data-path="${path}" class="mfs-editable-panel-title mfs-editable-component">Panel ${panelConf.key}</div>`;
    }

    render += `<div class="${layoutClass} ${alignClass} ${keyClass} mfs-panel ${
        panelConf.cssClass ?? ''
    }" >`;

    render += await renderContents(panelConf.contents, props, isTemplate, path);

    if (isTemplate) {
        render += `<div class="mfs-template-tab-controls">`;

        render += `<a class="item" title="Add new element"><i
                    class="fas fa-plus-circle mfs-clickable mfs-add-component" data-path="${path}"></i></a>`;

        render += `</div>`;
    }

    render += `</div>`;

    if (isTemplate) {
        render += `</div>`;
    }

    return render;
};

const renderTable = async (tableConf, props, isTemplate, path) => {
    let keyClass = tableConf.key ?? '';

    let rows = tableConf.rows ?? 1;
    let cols = tableConf.cols ?? 1;

    let render = '';

    render += `<table class="${keyClass} ${tableConf.cssClass ?? ''}">`;

    if (isTemplate) {
        render += `<thead><tr><th colspan="${cols}" class="mfs-cell-alignCenter mfs-editable-component" data-path="${path}">Table ${tableConf.key}</th></tr></thead>`;
    }

    render += `<tbody>`;

    for (let i = 0; i < rows; i++) {
        render += `<tr>`;
        for (let j = 0; j < cols; j++) {
            let cellAlignment = tableConf.layout
                ? tableConf.layout.substr(j, 1)
                : 'l';

            let cellClass;
            switch (cellAlignment) {
                case 'c':
                    cellClass = 'mfs-cell-alignCenter';
                    break;
                case 'r':
                    cellClass = 'mfs-cell-alignRight';
                    break;
                case 'l':
                default:
                    cellClass = 'mfs-cell-alignLeft';
                    break;
            }

            render += `<td class="mfs-cell ${cellClass}">`;

            if (tableConf.contents?.[i]?.[j]) {
                render += await renderComponent(
                    tableConf.contents[i][j],
                    props,
                    isTemplate,
                    path + '.contents.' + i + '.' + j
                );
            } else {
                if (isTemplate) {
                    render += `<div class="mfs-template-tab-controls">`;
                    render += `<a class="item" title="Add new element"><i class="fas fa-plus-circle mfs-clickable mfs-add-component" data-path="${
                        path + '.contents[' + i + ',' + j + ']'
                    }"></i></a>`;
                    render += `</div>`;
                } else {
                    render += await renderComponent(
                        {
                            type: 'label',
                            value: ''
                        },
                        props,
                        isTemplate,
                        path + '.contents.' + i + '.' + j
                    );
                }
            }

            render += `</td>`;
        }
        render += `</tr>`;
    }

    render += `</tbody></table>`;

    return render;
};

const renderDynamicTable = async (tableConf, props, isTemplate, path) => {
    let keyClass = tableConf.key ?? '';

    let render = `<table class="${keyClass} mfs-dynamicTable ${
        tableConf.cssClass ?? ''
    }" data-dynamic-table="${tableConf.key}">`;

    if (isTemplate) {
        render += `<thead><tr><th colspan="${
            tableConf.rowLayout?.length ? tableConf.rowLayout?.length + 1 : 1
        }" class="mfs-cell-alignCenter mfs-editable-component" data-path="${path}">Dynamic Table ${
            tableConf.key
        }</th></tr></thead>`;
    }

    render += `<tbody><tr>`;

    for (let i = 0; i < tableConf.rowLayout?.length ?? 0; i++) {
        let cellAlignment = tableConf.rowLayout[i].align ?? 'left';

        let cellClass;
        switch (cellAlignment) {
            case 'center':
                cellClass = 'mfs-cell-alignCenter';
                break;
            case 'right':
                cellClass = 'mfs-cell-alignRight';
                break;
            case 'left':
            default:
                cellClass = 'mfs-cell-alignLeft';
                break;
        }

        render += `<td class="mfs-cell ${cellClass} ${
            isTemplate ? 'mfs-editable-component' : ''
        }" data-path="${path + '.rowLayout.' + i}">${
            tableConf.head ? '<b>' : ''
        }${tableConf.rowLayout[i].colName} ${
            isTemplate ? '{' + tableConf.rowLayout[i].key + '}' : ''
        }${tableConf.head ? '</b>' : ''}</td>`;
    }

    render += `</td>`;

    if (isTemplate) {
        render += `<td class="mfs-cell mfs-cell-alignRight"><a class="item" title="Add new element"><i class="fas fa-plus-circle mfs-clickable mfs-add-dynamic-table-component" data-path="${path}"></i></a></td>`;
    }

    render += `</tr>`;

    let relevantRows = [];

    for (let rowIndex in props?.[tableConf.key]) {
        if (
            props[tableConf.key].hasOwnProperty(rowIndex) &&
            !props[tableConf.key][rowIndex]?.deleted
        ) {
            relevantRows.push(rowIndex);
        }
    }

    relevantRows = relevantRows.sort();

    for (let line of relevantRows) {
        render += `<tr class="mfs-dynamicRow" data-row-index="${line}">`;
        for (let j = 0; j < tableConf.rowLayout?.length ?? 0; j++) {
            let cellAlignment = tableConf.rowLayout[j].align ?? 'left';

            let cellClass;
            switch (cellAlignment) {
                case 'center':
                    cellClass = 'mfs-cell-alignCenter';
                    break;
                case 'right':
                    cellClass = 'mfs-cell-alignRight';
                    break;
                case 'left':
                default:
                    cellClass = 'mfs-cell-alignLeft';
                    break;
            }

            let component = {
                type: tableConf.rowLayout[j].type,
                key:
                    tableConf.key +
                    '.' +
                    line +
                    '.' +
                    tableConf.rowLayout[j].key,
                value:
                    props[tableConf.key][line][tableConf.rowLayout[j].key] ??
                    tableConf.rowLayout[j].value,
                options: tableConf.rowLayout[j].options ?? undefined,
                format: tableConf.rowLayout[j].format,
                maxLength: tableConf.rowLayout[j].maxLength,
                rollMessage: tableConf.rowLayout[j].rollMessage,
                style: tableConf.rowLayout[j].style,
                icon: tableConf.rowLayout[j].icon,
                size: tableConf.rowLayout[j].size,
                defaultValue: tableConf.rowLayout[j].defaultValue
            };

            render += `<td class="mfs-cell ${cellClass}">${await renderComponent(
                component ?? { type: 'label', value: '' },
                props
            )}</td>`;
        }
        render += `<td><div class="mfs-dynamic-table-row-icons"><span class="mfs-dynamic-table-sort-icon-wrapper">`;

        if (line !== relevantRows[0]) {
            render += `<a><i class="fas fa-sort-up mfs-dynamic-table-sort-icon mfs-sortUpDynamicLine mfs-clickable"></i></a>`;
        }

        if (line !== relevantRows[relevantRows.length - 1]) {
            render += `<a><i class="fas fa-sort-down mfs-dynamic-table-sort-icon mfs-sortDownDynamicLine mfs-clickable"></i></a>`;
        }

        render += `</span><a><i class="fas fa-trash mfs-deleteDynamicLine mfs-clickable"></i></a></div></td></tr>`;
    }

    if (!isTemplate) {
        render += `<tr><td colspan="${
            tableConf.rowLayout?.length ?? 0
        }"/><td><a><i class="fas fa-plus-circle mfs-addDynamicLine mfs-clickable"></i></a></td></tr>`;
    }

    render += `</tbody></table>`;
    return render;
};

const renderLabel = async (labelConf, props, isTemplate, path) => {
    let keyClass = labelConf.key ?? '';

    let labelTag;

    switch (labelConf.style) {
        case 'title':
            labelTag = 'h3';
            break;
        case 'label':
            labelTag = 'span';
            break;
        case 'subtitle':
            labelTag = 'h4';
            break;
        case 'bold':
            labelTag = 'b';
            break;
        default:
            labelTag = 'span';
            break;
    }

    let labelContent = isTemplate
        ? labelConf.value === ''
            ? '&#9744;'
            : labelConf.value
        : await _calculateLabelValue(labelConf.value, props, labelConf.key);

    if (labelConf.icon && labelConf.icon !== '') {
        labelContent = `<i class="fas fa-${labelConf.icon}"></i>${labelContent}`;
    }

    let render = `<${labelTag} class="${
        labelConf.style ? 'mfs-label-' + labelConf.style : ''
    } ${keyClass} ${isTemplate ? 'mfs-editable-component' : ''} ${
        labelConf.cssClass ?? ''
    } mfs-field mfs-field-${labelConf.size ?? 'full-size'}" data-path="${path}">${labelContent}</${labelTag}>`;

    if (labelConf.rollMessage) {
        let rollIcon = game.settings.get('modular-foundry-system', 'rollIcon');
        render = `<a class="mfs-rollable" data-roll-message="${
            labelConf.rollMessage
        }" data-reference="${labelConf.key}">${
            rollIcon !== ''
                ? '<i class="fas fa-' + rollIcon + '"></i>&nbsp;'
                : ''
        }${render}</a>`;
    }

    return render;
};

const renderCheckbox = (checkboxConf, props, isTemplate, path) => {
    let keyClass = checkboxConf.key ?? '';

    let render = `<span class="mfs-field mfs-checkbox mfs-field-${checkboxConf.size ?? 'full-size'} ${
        isTemplate ? 'mfs-editable-component' : ''
    } ${checkboxConf.cssClass ?? ''}" data-path="${path}">`;

    if (checkboxConf.label) {
        render += `<label
        for= "${checkboxConf.key}" >${checkboxConf.label} </label>`;
    }

    render += `<input type="checkbox" class="${keyClass}" id="${
        checkboxConf.key
    }" name="data.props.${checkboxConf.key}" data-path="${path}" ${
        props?.[checkboxConf.key] || checkboxConf.value
            ? 'checked="checked"'
            : ''
    } ${isTemplate ? 'onClick="return false;"' : ''}/>`;

    render += `</span>`;

    return render;
};

const renderTextField = (textFieldConf, props, isTemplate, path) => {
    let keyClass = textFieldConf.key ?? '';

    let render = `<span class="mfs-field mfs-text-field mfs-field-${textFieldConf.size ?? 'full-size'} ${
        isTemplate ? 'mfs-editable-component' : ''
    } ${textFieldConf.cssClass ?? ''}" data-path="${path}">`;

    if (textFieldConf.label) {
        render += `<label for="${textFieldConf.key}">${textFieldConf.label}</label>`;
    }

    render += `<input type="text" class="${keyClass}" id="${
        textFieldConf.key
    }" name="data.props.${textFieldConf.key}" value="${htmlEntities(
        textFieldConf.value ??
        props?.[textFieldConf.key] ??
        textFieldConf.defaultValue ??
        ''
    )}" ${isTemplate ? 'disabled="disabled"' : ''} data-format="${
        textFieldConf.format
    }" data-max-length="${textFieldConf.maxLength}"/>`;

    render += `</span>`;

    return render;
};

const renderTextArea = (textAreaConf, props, isTemplate, path) => {
    let keyClass = textAreaConf.key ?? '';

    let render = `<div class="mfs-field mfs-text-area ${
        isTemplate
            ? 'mfs-editable-component'
            : textAreaConf.style === 'sheet'
                ? 'mfs-rich-editor'
                : 'mfs-rich-editor-dialog'
    } ${keyClass} ${textAreaConf.cssClass ?? ''}" data-path="${path}">`;

    if (textAreaConf.label) {
        render += `<label for="${textAreaConf.key}">${textAreaConf.label}</label>`;
    }

    if (!isTemplate) {
        let contents =
            textAreaConf.value ??
            props?.[textAreaConf.key] ??
            textAreaConf.defaultValue ??
            '';
        switch (textAreaConf.style) {
            case 'sheet':
                let editor = $(
                    HandlebarsHelpers.editor({
                        hash: {
                            target: 'data.props.' + textAreaConf.key,
                            content: contents,
                            button: true,
                            editable: true
                        }
                    }).toHTML()
                );

                render += `${editor[0].outerHTML}`;
                break;
            case 'dialog':
                render += `
                    <div class="mfs-rich-content">${contents}</div>`;

                render += `<a class="mfs-rich-editor-button ${
                    contents !== '' ? 'mfs-rich-editor-button-float' : ''
                }" data-key="${
                    textAreaConf.key
                }"><i class="fas fa-edit"></i></a>`;

                break;
            case 'icon':
            default:
                render += `<a class="mfs-rich-editor-button" data-key="${textAreaConf.key}"><i class="fas fa-edit"></i></a>`;
                break;
        }
    } else {
        render += `${
            textAreaConf.defaultValue === ''
                ? '&#9744;'
                : textAreaConf.defaultValue
        }<i class="fas fa-paragraph"></i>`;
    }

    render += `</div>`;

    return render;
};

const renderSelect = (selectConf, props, isTemplate, path) => {
    let keyClass = selectConf.key ?? '';

    let value = props?.[selectConf.key] ?? selectConf.value ?? null;

    let render = `<span class="mfs-field mfs-select mfs-field-${selectConf.size ?? 'full-size'} ${
        isTemplate ? 'mfs-editable-component' : ''
    } ${selectConf.cssClass ?? ''}" data-path="${path}">`;

    if (selectConf.label) {
        render += `<label for="${selectConf.key}">${selectConf.label}</label>`;
    }

    render += `<select class="${keyClass}" id="${
        selectConf.key
    }" name="data.props.${selectConf.key}" ${
        isTemplate ? 'disabled="disabled"' : ''
    }>`;

    if (!selectConf.defaultValue) {
        render += `<option value=""></option>`;
    }

    for (let option of selectConf.options) {
        render += `<option value="${option.key}" ${
            value === option.key ||
            (!value && selectConf.defaultValue === option.key)
                ? 'selected'
                : ''
        }>${option.value}</option>`;
    }

    render += `</select>`;

    render += `</span>`;

    return render;
};

const _calculateLabelValue = async (label, props, reference) => {
    let phrase = await ComputablePhrase.computeMessage(label, props, {
        reference,
        defaultValue: ''
    });
    return phrase.result;
};

function htmlEntities(str) {
    return String(str)
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
}
