import { renderTab } from './tab-renderer.mjs';

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class ModularActorSheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ['modular', 'sheet', 'actor'],
            template:
                'systems/modular-foundry-system/templates/actor/actor-sheet.html',
            width: 600,
            height: 600,
            tabs: [
                {
                    navSelector: '.sheet-tabs',
                    contentSelector: '.sheet-body',
                    initial: 'features'
                }
            ]
        });
    }

    /** @override */
    get template() {
        return `systems/modular-foundry-system/templates/actor/actor-${this.actor.data.type}-sheet.html`;
    }

    /* -------------------------------------------- */

    /** @override */
    getData() {
        // Retrieve the data structure from the base sheet. You can inspect or log
        // the context variable to see the structure, but some key properties for
        // sheets are the actor object, the data object, whether or not it's
        // editable, the items array, and the effects array.
        const context = super.getData();

        // Use a safe clone of the actor data for further operations.
        const actorData = context.actor.data;

        // Add the actor's data to context.data for easier access, as well as flags.
        context.data = actorData.data;
        context.flags = actorData.flags;

        // Add roll data for TinyMCE editors.
        context.rollData = context.actor.getRollData();

        return context;
    }

    /**
     * Pre-renders sheet contents
     * @param context
     * @param isTemplate
     * @protected
     */
    async _preRenderSheet(context, isTemplate) {
        // Render header like any tab
        let header = await renderTab(
            context.data.header?.contents,
            context.data.props,
            isTemplate,
            'header'
        );

        let tabs = [];
        let tabsContent = {};

        if (context.data.tabs) {
            for (let idx of context.data.tabs.keys()){
                let tab = context.data.tabs[idx];

                // Fetch tab details for the tab selector
                tabs.push({ key: tab.key, name: tab.name });

                // Render all tabs
                tabsContent[tab.key] = await renderTab(
                    tab.contents,
                    context.data.props,
                    isTemplate,
                    'tabs.' + idx
                );
            }
        }

        context.headerPanel = header;
        context.tabs = tabs;
        context.tabsContent = tabsContent;

        context.isGM = game.user.isGM;
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
    }

    /**
     * Gets prop value with a complex path (with . as separator)
     * @param path
     * @returns {*}
     * @protected
     */
    _getDeepProp(path) {
        let part = this.actor.data.data.props;

        let splitPath = path.split('.');

        for (let pathPart of splitPath) {
            if (part) {
                part = part[pathPart];
            } else {
                break;
            }
        }

        return part;
    }
}
