import { ModularActorSheet } from './actor-sheet.mjs';
import ComputablePhrase from '../formulas/computablePhrase.mjs';
import { postAugmentedChatMessage } from '../utils.mjs';

/**
 * @extends {ModularActorSheet}
 */
export class CharacterSheet extends ModularActorSheet {
    /* -------------------------------------------- */

    /** @override */
    async getData() {
        // Retrieve the data structure from the base sheet. You can inspect or log
        // the context variable to see the structure, but some key properties for
        // sheets are the actor object, the data object, whether or not it's
        // editable, the items array, and the effects array.
        const context = super.getData();

        // Prepare character data and items.
        await this._prepareSheetData(context);

        return context;
    }

    /**
     * Organize and classify Items for Character sheets.
     *
     * @param {Object} actorData The actor to prepare.
     *
     * @return {undefined}
     */
    async _prepareSheetData(context) {
        context.availableTemplates = game.actors.filter(
            (actor) => actor.data.type === '_template'
        );

        await super._preRenderSheet(context, false);
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // -------------------------------------------------------------
        // Everything below here is only needed if the sheet is editable
        if (!this.isEditable) return;

        // Template reload button
        html.find('.mfs-template-select #mfs-reload-template').click((ev) => {
            if(game.user.isGM) {
                const target = $(ev.currentTarget);
                const templateId = target
                    .parents('.mfs-template-select')
                    .find('#template')
                    .val();

                this.reloadTemplate(templateId);
            }
        });

        // Dynamic line button delete
        html.find('.mfs-deleteDynamicLine').click((ev) => {
            const row = $(ev.currentTarget).parents('.mfs-dynamicRow');
            const table = $(ev.currentTarget).parents('.mfs-dynamicTable');

            // Fetch row localization data
            const tableId = table.data('dynamic-table');
            const rowId = row.data('row-index');

            let props = { ...this.actor.data.data.props };

            // We can not directly delete the object, for some reason it won't save
            // So we flag it as deleted
            props[tableId][rowId].deleted = true;

            this.actor
                .update({
                    data: {
                        props: props
                    }
                })
                .then(() => {
                    console.debug('Updated !');
                });
            row.slideUp(1000, () => this.render(false));
        });

        // Add dynamic line button
        html.find('.mfs-addDynamicLine').click((ev) => {
            const table = $(ev.currentTarget).parents('.mfs-dynamicTable');

            const dynamicRows = table.find('.mfs-dynamicRow');

            // Fetch new row localization data
            // New row index is calculated by last row index + 1, or 0 if no rows exist
            const tableId = table.data('dynamic-table');
            const lastRowIndex = dynamicRows.last()?.data('row-index') ?? -1;

            // Attempt to get old row if exists, to empty it
            // Old row will only exist if deleted
            let oldVal =
                this.actor.data.data?.props?.[tableId]?.[lastRowIndex + 1];

            if (oldVal) {
                for (let prop in oldVal) {
                    oldVal[prop] = null;
                }
            } else {
                oldVal = {};
            }

            // Create prop for dynamic table if not exists (can happen at character creation)
            if (!this.actor.data.data?.props?.[tableId]) {
                this.actor.data.data.props[tableId] = {};
            }

            // Set new row inside dynamic table data
            this.actor.data.data.props[tableId][lastRowIndex + 1] = oldVal;

            this.actor
                .update({
                    data: {
                        props: this.actor.data.data.props
                    }
                })
                .then(() => {
                    console.debug('Updated !');
                });

            this.render(false);
        });

        // Dynamic table sort up button
        html.find('.mfs-sortUpDynamicLine').click((ev) => {
            const row = $(ev.currentTarget).parents('.mfs-dynamicRow');
            const table = $(ev.currentTarget).parents('.mfs-dynamicTable');

            // Fetch row localization data
            const tableId = table.data('dynamic-table');
            const rowId = row.data('row-index');

            let props = { ...this.actor.data.data.props };

            // Fetch not deleted row index => displayed rows
            let relevantRows = [];

            for (let rowIndex in props[tableId]) {
                if (!props[tableId][rowIndex].deleted) {
                    relevantRows.push(rowIndex);
                }
            }

            // Sort rows, just in case
            relevantRows = relevantRows.sort();

            // Switch places between current row and previous row, if previous row exists
            let rowPosition = relevantRows.indexOf(String(rowId));

            if (rowPosition !== 0) {
                let prevRow = {
                    ...props[tableId][relevantRows[rowPosition - 1]]
                };

                props[tableId][relevantRows[rowPosition - 1]] =
                    props[tableId][relevantRows[rowPosition]];
                props[tableId][relevantRows[rowPosition]] = prevRow;
            }

            this.actor
                .update({
                    data: {
                        props: props
                    }
                })
                .then(() => {
                    console.debug('Updated !');
                });
            this.render(false);
        });

        // Dynamic table sort down button
        html.find('.mfs-sortDownDynamicLine').click((ev) => {
            const row = $(ev.currentTarget).parents('.mfs-dynamicRow');
            const table = $(ev.currentTarget).parents('.mfs-dynamicTable');

            // Fetch row localization data
            const tableId = table.data('dynamic-table');
            const rowId = row.data('row-index');

            let props = { ...this.actor.data.data.props };

            // Fetch not deleted row index => displayed rows
            let relevantRows = [];

            for (let rowIndex in props[tableId]) {
                if (!props[tableId][rowIndex].deleted) {
                    relevantRows.push(rowIndex);
                }
            }

            // Sort rows, just in case
            relevantRows = relevantRows.sort();

            // Switch places between current row and next row, if next row exists
            let rowPosition = relevantRows.indexOf(String(rowId));

            if (rowPosition !== relevantRows.length - 1) {
                let nextRow = {
                    ...props[tableId][relevantRows[rowPosition + 1]]
                };

                props[tableId][relevantRows[rowPosition + 1]] =
                    props[tableId][relevantRows[rowPosition]];
                props[tableId][relevantRows[rowPosition]] = nextRow;
            }

            this.actor
                .update({
                    data: {
                        props: props
                    }
                })
                .then(() => {
                    console.debug('Updated !');
                });
            this.render(false);
        });

        // Post a chat message with roll data
        html.find('.mfs-rollable').click((ev) => {
            const rollMessage = $(ev.currentTarget).data('roll-message');

            // Reference is fetched for roll-messages in dynamic tables
            const labelKey = $(ev.currentTarget).data('reference');

            // Compute the chat-message final value
            let textContent = new ComputablePhrase(rollMessage);
            textContent.compute(this.actor.data.data.props, {
                reference: labelKey
            }).then(() => {
                postAugmentedChatMessage(textContent);
            });
        });

        // Rich editor dialog open button
        html.find('.mfs-rich-editor-button').click((ev) => {
            const target = $(ev.currentTarget);

            // Get property data for edition
            const key = target.data('key');
            const title = target.data('title') ?? 'Text editor';
            const data = this._getDeepProp(key) ?? '';

            // Content of the dialog is just this textarea, which will be used to create a rich editor inside the dialog
            let content = `<textarea id='mfs-rich-text-editor-${key}' class='mfs-rich-text-editor'>${data}</textarea>`;

            // Dialog creation
            let d = new Dialog(
                {
                    title: title,
                    content: content,
                    buttons: {
                        validate: {
                            icon: '<i class="fas fa-check"></i>',
                            label: 'Save',
                            callback: (html) => {
                                // Recover rich text editor content and save it to the right prop
                                this.actor.data.data.props[key] = tinymce
                                    .get(`mfs-rich-text-editor-${key}`)
                                    .getContent();
                                this.actor
                                    .update({
                                        data: {
                                            props: this.actor.data.data.props
                                        }
                                    })
                                    .then(() => {
                                        console.log('Updated !');
                                    });
                                this.render(false);
                            }
                        },
                        cancel: {
                            icon: '<i class="fas fa-times"></i>',
                            label: 'Cancel'
                        }
                    },
                    render: () => {
                        //Pre-emptively remove editors to guarantee init
                        tinymce.remove('textarea.mfs-rich-text-editor');
                        tinymce.init({
                            ...CONFIG.TinyMCE,
                            selector: 'textarea.mfs-rich-text-editor'
                        });
                    }
                },
                {
                    width: 500,
                    height: 280
                }
            );
            d.render(true);
        });

        // Handler to show the hover dialog button over rich text fields
        html.find('.mfs-rich-editor-dialog')
            .mouseover((ev) => {
                let target = $(ev.currentTarget);
                let icon = target.find('.mfs-rich-editor-button-float');
                icon.css('display', 'inline');
            })
            .mouseleave((ev) => {
                let target = $(ev.currentTarget);
                let icon = target.find('.mfs-rich-editor-button-float');
                icon.css('display', 'none');
            });

        html.find('.mfs-text-field').keydown((ev) => {
            let target = $(ev.currentTarget);
            let input = target.find('input');

            let oldValue = input.val();

            input.one('change', (ev) => {
                let format = input.data('format');
                let maxLength = input.data('max-length');

                let value = input.val();

                if (maxLength && maxLength > 0 && value.length > maxLength) {
                    value = value.substring(0, maxLength);
                }

                switch (format) {
                    case 'integer':
                        if (!Number.isInteger(Number(value))) {
                            value = oldValue;
                        }
                        break;
                    case 'numeric':
                        if (isNaN(value)) {
                            value = oldValue;
                        }
                        break;
                    default:
                        break;
                }

                input.val(value);
            });


        });
    }

    reloadTemplate (templateId = null) {
        templateId = templateId || this.actor.data.data.template;

        const template = game.actors.get(templateId);

        // Updates hidden properties, tabs & header data
        // Sheet rendering will handle the actual props creation
        this.actor
            .update({
                data: {
                    template: templateId,
                    hidden: template.data.data.hidden,
                    tabs: template.data.data.tabs,
                    header: template.data.data.header
                }
            })
            .then(() => {
                console.debug('Updated !');
            });

        this.render(false);

    }

}
