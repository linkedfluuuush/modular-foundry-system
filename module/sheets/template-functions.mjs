/**
 * Dialog for tab creation / edition
 * @param callback The callback to call on Save click
 * @param tabData The existing Tab data, if any
 * @returns {Promise<void>}
 */
const editTab = async (callback, tabData) => {
    // Render the dialog contents
    let content = await renderTemplate(
        `systems/modular-foundry-system/templates/_template/dialogs/edit-tab.html`,
        tabData
    );

    // Create the dialog
    let d = new Dialog({
        title: 'New tab',
        content: content,
        buttons: {
            validate: {
                icon: '<i class="fas fa-check"></i>',
                label: 'Save tab',
                callback: (html) => {
                    // Ensure name and key are entered
                    let name = html.find('#tab-name').val();
                    let key = html.find('#tab-key').val();

                    if (!name || !key) {
                        throw new Error('Name and Key must not be empty');
                    } else {
                        callback(name, key);
                    }
                }
            },
            cancel: {
                icon: '<i class="fas fa-times"></i>',
                label: 'Cancel'
            }
        },
        default: 'cancel'
    });
    d.render(true);
};

/**
 * Dialog for component creation / edition
 * @param callback The callback to call on button click
 * @param componentData The existing component's data, if any
 * @returns {Promise<void>}
 */
const componentDialog = async (callback, componentData) => {
    // If component data contains options, we transform it to fit the display
    if (componentData?.options) {
        componentData.options_formatted = componentData.options
            .reduce((acc, val) => {
                return acc + val.key + ' - ' + val.value + '\n';
            }, '')
            .slice(0, -1);
    }

    // Render the dialog's contents
    let content = await renderTemplate(
        `systems/modular-foundry-system/templates/_template/dialogs/component.html`,
        componentData
    );

    let editButtons = {};

    // If component data was provided, we can display the edit actions : Delete and Sort buttons
    if (componentData) {
        editButtons = {
            delete: {
                icon: '<i class="fas fa-trash"></i>',
                label: 'Delete',
                callback: () => {
                    // Just call callback with delete, and no new component data
                    callback('delete');
                }
            },
            moveBefore: {
                icon: '<i class="fas fa-caret-left"></i>',
                label: 'Move before',
                callback: () => {
                    // Just call callback with sortUp, and no new component data
                    callback('sortUp');
                }
            },
            moveAfter: {
                icon: '<i class="fas fa-caret-right"></i>',
                label: 'Move after',
                callback: () => {
                    // Just call callback with sortDown, and no new component data
                    callback('sortDown');
                }
            }
        };
    }

    // Create dialog
    let d = new Dialog({
        title: 'Edit component',
        content: content,
        buttons: {
            validate: {
                icon: '<i class="fas fa-check"></i>',
                label: 'Save',
                callback: (html) => {
                    // Remove all editors to save rich text into the original textareas
                    tinymce.remove('.mfs-component-editor-dialog textarea');

                    let fieldData = {};

                    // Fetch fields existing for every type of components
                    let genericFields = html.find(
                        '.mfs-component-generic-fields input, .mfs-component-generic-fields select, .mfs-component-generic-fields textarea'
                    );

                    // Store their value in an object
                    for (let field of genericFields) {
                        let jQField = $(field);
                        fieldData[jQField.data('key')] = jQField.val();
                    }

                    // Fetch type specific fields
                    let compFields = html.find(
                        '.mfs-' +
                        fieldData.type +
                        '-editor input, .mfs-' +
                        fieldData.type +
                        '-editor select, .mfs-' +
                        fieldData.type +
                        '-editor textarea'
                    );

                    // Store their value in an object
                    for (let field of compFields) {
                        let jQField = $(field);
                        // If field is checkbox, value is not stored in val(), but must be recovered using is:checked
                        fieldData[jQField.data('key')] =
                            jQField.prop('type') === 'checkbox'
                                ? jQField.is(':checked')
                                : jQField.val();
                    }

                    // If new field is select, we transform its options into the final object
                    if (fieldData.type === 'select') {
                        let optionsArray = fieldData.options.split('\n');

                        fieldData.options = optionsArray.map((opt) => {
                            // We recover the first part as key, the rest is the text
                            let splitOpt = opt.split(' - ');
                            let optKey = splitOpt.shift();
                            let optVal = splitOpt.join(' - ');

                            return {
                                key: optKey,
                                value: optVal
                            };
                        });
                    }

                    callback('edit', fieldData);
                }
            },
            ...editButtons,
            cancel: {
                icon: '<i class="fas fa-times"></i>',
                label: 'Cancel'
            }
        }
    });
    d.render(true);
};

/**
 * Dialog for hidden attributes creation / edition
 * @param callback The callback to call on button click
 * @param attr The existing hidden attributes
 * @returns {Promise<void>}
 */
const attributesDialog = async (callback, attr) => {
    // Render the dialog's contents
    let content = await renderTemplate(
        `systems/modular-foundry-system/templates/_template/dialogs/attributes.html`,
        { attribute: attr }
    );

    // Create dialog
    let d = new Dialog({
        title: 'Edit attributes',
        content: content,
        buttons: {
            validate: {
                icon: '<i class="fas fa-check"></i>',
                label: 'Save',
                callback: (html) => {
                    // Fetch all attribute rows
                    let attrEltList = html.find('tr.mfs-hidden-attribute');
                    let attrList = [];

                    // For each of them, recover key and formula, and ensure none is empty
                    for (let attrElt of attrEltList) {
                        let attrName = $(attrElt)
                            .find('.mfs-attribute-name')
                            .val();
                        let attrFormula = $(attrElt)
                            .find('.mfs-attribute-formula')
                            .val();

                        if (attrName === '' || attrFormula === '') {
                            throw new Error(
                                'Name and Formula must be entered for each attribute'
                            );
                        }

                        attrList.push({ name: attrName, value: attrFormula });
                    }

                    callback(attrList);
                }
            },
            cancel: {
                icon: '<i class="fas fa-times"></i>',
                label: 'Cancel'
            }
        }
    });
    d.render(true);
};

export default {
    editTab,
    componentDialog,
    attributesDialog
};
