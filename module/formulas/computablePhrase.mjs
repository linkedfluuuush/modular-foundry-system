import Formula from './formula.mjs';

/**
 * Class holding computed phrase details, for explanation
 */
export default class ComputablePhrase {
    _rawPhrase;
    _buildPhrase;
    _computedFormulas = {};

    constructor(phrase) {
        this._rawPhrase = phrase;
    }

    /**
     * Gets the raw formula
     * @returns {*}
     */
    get formula() {
        let phrase = this._buildPhrase;
        for (let key in this._computedFormulas) {
            phrase = phrase.replaceAll(key, this._computedFormulas[key].raw);
        }

        return phrase;
    }

    /**
     * Gets the computed formula, i.e. the raw formula with all token replaced by their values
     * @returns {*}
     */
    get parsed() {
        let phrase = this._buildPhrase;
        for (let key in this._computedFormulas) {
            phrase = phrase.replaceAll(key, this._computedFormulas[key].parsed);
        }

        return phrase;
    }

    /**
     * Gets the phrase ready for replacements
     * @returns {*}
     */
    get buildPhrase() {
        return this._buildPhrase;
    }

    /**
     * Gets the resulting phrase, i.e. the fully computed values
     * @returns {*}
     */
    get result() {
        let phrase = this._buildPhrase;
        for (let key in this._computedFormulas) {
            phrase = phrase.replaceAll(key, this._computedFormulas[key].result);
        }

        return phrase;
    }

    get values() {
        return this._computedFormulas;
    }

    /**
     *
     * @param props
     * @param {{reference: string, defaultValue: string}} options
     */
    async compute(props, options = {}) {
        console.debug('Computing ' + this._rawPhrase);

        let phrase = this._rawPhrase;

        let localVars = {};

        let computedFormulas = {};
        let nComputed = 0;

        // Get every formula in the phrase. Formulas are surrounded by ${ and }$ for easy delimitation
        let formulaIterator = phrase.matchAll(/\${.*?}\$/g);
        let textFormula = formulaIterator.next();
        while (!textFormula.done) {
            let formula = new Formula(
                textFormula.value[0].substring(2).slice(0, -2)
            );

            await formula.compute(props, {
                localVars,
                ...options
            });

            // Saves formula data
            let computedId = 'form' + nComputed;
            computedFormulas[computedId] = formula;

            phrase = phrase.split(textFormula.value[0]).join(computedId);

            localVars = {
                ...localVars,
                ...formula.localVars
            }

            nComputed++;
            textFormula = formulaIterator.next();
        }

        this._buildPhrase = phrase;
        this._computedFormulas = computedFormulas;
    }

    /**
     *
     * @param props
     * @param {{reference: string, defaultValue: string}} options
     */
    computeStatic(props, options = {}) {
        console.debug('Computing ' + this._rawPhrase);

        let phrase = this._rawPhrase;

        let localVars = {};

        let computedFormulas = {};
        let nComputed = 0;

        // Get every formula in the phrase. Formulas are surrounded by ${ and }$ for easy delimitation
        let formulaIterator = phrase.matchAll(/\${.*?}\$/g);
        let textFormula = formulaIterator.next();
        while (!textFormula.done) {
            let formula = new Formula(
                textFormula.value[0].substring(2).slice(0, -2)
            );

            formula.computeStatic(props, {
                localVars,
                ...options
            });

            // Saves formula data
            let computedId = 'form' + nComputed;
            computedFormulas[computedId] = formula;

            phrase = phrase.split(textFormula.value[0]).join(computedId);

            localVars = {
                ...localVars,
                ...formula.localVars
            }

            nComputed++;
            textFormula = formulaIterator.next();
        }

        this._buildPhrase = phrase;
        this._computedFormulas = computedFormulas;
    }

    /**
     * Computes every formulas in one phrase
     * @param phrase
     * @param props
     * @param options
     * @returns {ComputablePhrase}
     */
    static async computeMessage(phrase, props, options = {}) {
        let computablePhrase = new ComputablePhrase(phrase);
        await computablePhrase.compute(props, options);

        return computablePhrase;
    }

    /**
     * Computes every formulas in one phrase, without rolls or user inputs
     * @param phrase
     * @param props
     * @param options
     * @returns {ComputablePhrase}
     */
    static computeMessageStatic(phrase, props, options = {}) {
        let computablePhrase = new ComputablePhrase(phrase);
        computablePhrase.computeStatic(props, options);

        return computablePhrase;
    }


    toString() {
        return this.result;
    }
}
