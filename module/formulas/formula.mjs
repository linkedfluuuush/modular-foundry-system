import { UncomputableError } from '../errors/errors.mjs';
import ComputablePhrase from './computablePhrase.mjs';

/**
 * Class holding computed phrase details, for explanation
 */
export default class Formula {
    _raw;
    _result;
    _localVars;
    _parsed;
    _hasDice;
    _tokens;
    _rolls;
    _hidden = false;

    constructor(formula) {
        this._raw = formula;
    }

    get raw() {
        return this._raw;
    }

    get result() {
        return this._result;
    }

    get localVars() {
        return this._localVars;
    }

    get parsed() {
        return this._parsed;
    }

    get hasDice() {
        return this._hasDice;
    }

    get tokens() {
        return this._tokens;
    }

    get rolls() {
        return this._rolls;
    }

    get hidden() {
        return this._hidden;
    }

    toJSON() {
        return {
            raw: this.raw,
            result: this.result,
            parsed: this.parsed,
            hasDice: this.hasDice,
            tokens: this.tokens,
            rolls: this.rolls,
            hidden: this.hidden
        };
    }

    /**
     * Computes this formula with given props and options
     * @param props Token attributes to replace inside the formula
     * @param {{reference: string, defaultValue: string, localVars: {}}} options
     * @returns {Promise <void>}
     */
    async compute(props, options = {}) {
        // Reference is used to compute formulas in dynamic table, to reference a same-line data
        // Default value is used in case a token is not computable
        // Local vars are used to re-use previously defined vars in the phrase
        let { localVars = {} } = options ?? {};

        console.debug('Computing rolls & user inputs in ${' + this._raw + '}$');

        let { formula, textVars } = handleTextVars(this._raw);

        // Rolls are formula-local tokens which hold the roll data
        let rolls = [];

        // If formula starts with #, it should not be visible by default
        if (formula.startsWith('#')) {
            this._hidden = true;
            formula = formula.substring(1);
        }

        // Isolating user inputs, enclosed in ?{} inside the formula
        let userInputTokens = formula.matchAll(/\?{.*?}/g);
        let userInputToken = userInputTokens.next();

        let allUserVars = [];

        while (!userInputToken.done) {
            let userInputName = userInputToken.value[0].substring(2).slice(0, -1);
            allUserVars.push(userInputName);

            formula = formula.replace(userInputToken.value[0], userInputName);

            userInputToken = userInputTokens.next();
        }

        if (allUserVars.length > 0) {
            let content = await renderTemplate(
                `systems/modular-foundry-system/templates/_template/dialogs/user-input.html`,
                { allUserVars: allUserVars }
            );

            let userData = await new Promise((resolve) => {
                Dialog.prompt({
                    content: content,
                    callback: (html) => {
                        let values = {};
                        let inputs = $(html).find('input.mfs-user-input');

                        for (let elt of inputs) {
                            values[$(elt).data('var-name')] = $(elt).val();
                        }

                        resolve(values);
                    },
                    rejectClose: false
                });
            });

            localVars = { ...localVars, ...userData };
        }

        // Handling rolls - rolls are enclosed in brackets []
        let rollMessages = formula.matchAll(/\[.+?]/g);
        let roll = rollMessages.next();
        while (!roll.done) {
            // Evaluating roll with Foundry VTT Roll API
            let rollString = roll.value[0];

            console.debug('\tRolling ' + rollString);

            let rollResult = await this.evaluateRoll(
                rollString.substr(1).slice(0, -1),
                props,
                options
            );

            // Replacing roll result in formula for computing and saving roll data for display in chat message
            formula = formula.replace(
                rollString,
                rollResult.total
            );

            let rollFormula = rollString === '[' + rollResult.formula + ']' ? rollString : rollString + ' → [' + rollResult.formula + ']';

            rolls.push({ formula: rollFormula, roll: rollResult.toJSON() });

            roll = rollMessages.next();
        }

        this.computeStatic(props, {
            ...options,
            localVars,
            textVars,
            rolls
        }, formula);
    }

    /**
     * Computes this formula with given props and options
     * @param props Token attributes to replace inside the formula
     * @param {{reference: string, defaultValue: string, localVars: {}}} options
     * @param formula
     */
    computeStatic(props, options = {}, formula = null) {
        formula = formula ?? this._raw;
        console.debug('Computing ${' + formula + '}$');

        let { reference, defaultValue, localVars = {}, textVars = {}, rolls = [] } = options;

        // Detecting local variable to set
        let localVarName = null;
        let localVarDecomposed = formula.match(
            /^([a-zA-Z0-9_-]+):=(.*)$/
        );

        if (localVarDecomposed) {
            localVarName = localVarDecomposed[1];
            formula = localVarDecomposed[2];
        }

        // If text-vars exist, they have already been handled ; no need to do it again
        if (Object.keys(textVars).length === 0) {
            let textVarResult = handleTextVars(formula);

            formula = textVarResult.formula;
            textVars = textVarResult.textVars;
        }

        // Stripping formula from remaining spaces to have a consistent parsable string
        let strippedFormula = formula.replaceAll(' ', '').trim();

        // Fetching the rest of the tokens
        let tokens = strippedFormula.matchAll(/@?[$A-Za-z0-9_]+/g);
        let computedTokens = {};
        let renamedTokens = {};

        let canBeNumber = true;

        let token = tokens.next();
        let nTransformedNames = 0;
        while (!token.done) {
            let tokenString = token.value[0];

            // If token has already been computed, no need to recompute it
            if (!computedTokens[tokenString]) {
                console.debug('\tComputing ' + tokenString);

                let isReference = false;

                // If starts with $, the value in a reference to another field in which we should take the value
                if(tokenString.startsWith('@')){
                    isReference = true;
                    tokenString = tokenString.substring(1);
                }

                // If starts with $, it references a same-line prop in a dynamic table
                if (tokenString.startsWith('$')) {
                    // Reference is 'dynamicTableKey.rowNumber'
                    let tokenPath = reference.split('.');

                    let cleanedTokenString = tokenString.substr(1);

                    // Fetching the value inside dynamic table's row
                    let tokenVal =
                        props[tokenPath[0]][tokenPath[1]][cleanedTokenString];

                    // If result is string, computing formula as encoded HTML string
                    if (isNaN(tokenVal)) {
                        canBeNumber = false;
                        computedTokens[tokenString] = prepareString(tokenVal);
                    } else {
                        computedTokens[tokenString] = tokenVal;
                    }
                } else if (tokenString.includes('$')) {
                    // If it contains an @ not in first position, this is a reference to a dynamic table column
                    let dynamicTable = tokenString.split('$')[0];
                    let rowIndex = tokenString.split('$')[1];

                    let values = [];

                    if (props[dynamicTable] !== undefined) {
                        for (let row in props[dynamicTable]) {
                            if (!props[dynamicTable][row].deleted) {
                                values.push(isNaN(props[dynamicTable][row][rowIndex]) ? 0 : Number(props[dynamicTable][row][rowIndex]));
                            }
                        }
                    }

                    computedTokens[tokenString] = values;
                } else if (localVars[tokenString] !== undefined) {
                    // If token is a local var, fetch it from local vars
                    if (localVars[tokenString] === null) {
                        computedTokens[tokenString] = '';
                    } else {
                        if (isNaN(localVars[tokenString])) {
                            canBeNumber = false;
                            computedTokens[tokenString] = prepareString(localVars[tokenString]);
                        } else {
                            computedTokens[tokenString] =
                                localVars[tokenString];
                        }
                    }
                } else if (props[tokenString] !== undefined) {
                    // If token is a prop, fetch it from props
                    if (props[tokenString] === null) {
                        computedTokens[tokenString] = '';
                    } else {
                        if (isNaN(props[tokenString])) {
                            canBeNumber = false;
                            computedTokens[tokenString] = prepareString(props[tokenString]);
                        } else {
                            computedTokens[tokenString] = props[tokenString];
                        }
                    }
                } else if (textVars[tokenString] !== undefined) {
                    // If token is a previously detected text, fetch it from text vars
                    computedTokens[tokenString] = textVars[tokenString];
                } else if (
                    // If token is a math function, use it as is
                    math[tokenString] &&
                    math[tokenString] instanceof Function
                ) {

                } else if (!isNaN(tokenString)) {

                } else if (defaultValue !== undefined) {
                    // If nothing was found but a default value is set, use default.
                    // This is used on rendering, to allow for empty fields
                    computedTokens[tokenString] = defaultValue;
                } else {
                    // If all else fails, throw error
                    throw new UncomputableError(
                        'Uncomputable token ' + tokenString,
                        tokenString,
                        formula,
                        props
                    );
                }

                if(isReference){
                    let realToken = '@' + tokenString;
                    computedTokens[computedTokens[tokenString]] = props[computedTokens[tokenString]] ||  '';
                    let transformedName = '_reference_' + nTransformedNames;
                    nTransformedNames++;
                    renamedTokens[transformedName] = computedTokens[computedTokens[tokenString]];
                    strippedFormula = strippedFormula.replace(realToken, transformedName);
                }

                console.debug(
                    '\t\t => ' +
                    tokenString +
                    '="' +
                    computedTokens[tokenString] +
                    '"'
                );
            }

            token = tokens.next();
        }

        let mathTokens = { ...computedTokens, ...renamedTokens };
        console.debug({ formula: strippedFormula, scope: mathTokens });

        let result;

        try {
            result = math.evaluate(strippedFormula, mathTokens);
        } catch (err) {
            result = 'ERROR';
            console.error(err);
        }

        if (localVarName) {
            localVars[localVarName] = result;
        }

        // Save every detail of the computation
        this._result = result;
        this._localVars = localVars;
        this._parsed = strippedFormula;
        this._hasDice = rolls.length > 0;
        this._tokens = computedTokens;
        this._rolls = rolls;
    }

    /**
     * Evaluates a roll expression through Foundry VTT Roll API
     * @param rollText
     * @param props
     * @param options
     * @returns {Roll}
     */
    async evaluateRoll(rollText, props, options) {
        // Roll can contain parameters delimited by colons (:)
        let rollTextParamMatcher = rollText.matchAll(/:(.*?):/g);
        let rollTextParam = rollTextParamMatcher.next();

        // Start by building a temporary phrase with every found parameter
        // A roll like [1d100 + STR] will become [1d100 + ${STR}$], which can be computed like other formulas
        while (!rollTextParam.done) {
            rollText = rollText.replace(
                rollTextParam.value[0],
                `\${${rollTextParam.value[1]}}\$`
            );
            rollTextParam = rollTextParamMatcher.next();
        }

        // Temporary phrase is computed to get a number & dice only phrase
        let finalRollText = new ComputablePhrase(rollText);
        await finalRollText.compute(props, options);

        // Roll evaluation
        let roll = new Roll(finalRollText.result);
        await roll.evaluate();

        return roll;
    }
}

const prepareString = (s) => {
    if (s) {
        if (s.startsWith('\'') && s.endsWith('\'')) {
            s = s.substr(1).slice(0, -1);
        }

        s = s.replaceAll('\\\'', '\'');

        return s;
    } else {
        return '';
    }
};

/**
 * Handles text variables by extracting them and replacing them with tokens
 * @param formula
 * @returns {{textVars: {}, formula}}
 */
const handleTextVars = (formula) => {
    // Text vars are formula-local tokens which hold the texts
    let textVars = {};

    // Isolating text data, enclosed in '' inside the formula
    // The (?<!\\)' part means match quotes (') which are not preceded by \
    let textTokens = formula.matchAll(/(?<!\\)'.*?(?<!\\)'/g);
    let textToken = textTokens.next();

    while (!textToken.done) {
        let textRef = '_computedText_' + (Object.keys(textVars).length + 1);

        // Recreate apostrophes inside text + removing delimiters
        textVars[textRef] = prepareString(textToken.value[0]);

        formula = formula.replace(textToken.value[0], textRef);

        textToken = textTokens.next();
    }

    return { formula, textVars };
};