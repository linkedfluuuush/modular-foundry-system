// Import document classes.
import { ModularActor } from './documents/actor.mjs';
// Import sheet classes.
import { CharacterSheet } from './sheets/character-sheet.mjs';
import { TemplateSheet } from './sheets/template-sheet.mjs';
import ComputablePhrase from './formulas/computablePhrase.mjs';
import Formula from './formulas/formula.mjs';
import { postAugmentedChatMessage } from './utils.mjs';

/* -------------------------------------------- */
/*  Init Hook                                   */
/* -------------------------------------------- */

Hooks.once('init', function() {
    // Define custom Document classes
    CONFIG.Actor.documentClass = ModularActor;

    game.settings.register('modular-foundry-system', 'initFormula', {
        name: 'Initiative Formula',
        hint: 'After editing, please refresh instance',
        scope: 'world',
        config: true,
        default: '[[1d20]]',
        type: String
    });

    // Register system settings
    game.settings.register('modular-foundry-system', 'customStyle', {
        name: 'CSS Style file',
        hint: 'You can specify a custom styling file. If default wanted, leave blank',
        scope: 'world',
        config: true,
        default: '',
        type: String
    });

    game.settings.register('modular-foundry-system', 'expandRollVisibility', {
        name: 'Expand Roll Visibility to whole Chat messages',
        hint: 'If checked, roll visibility will affect the whole message. This works using whispers.',
        scope: 'world',
        config: true,
        default: '',
        type: Boolean
    });

    // Register system settings
    game.settings.register('modular-foundry-system', 'rollIcon', {
        name: 'Roll Icons',
        hint: 'You can specify a fontawesome icon name to add an icon next to the clickable labels in the sheet. Example : dice-d20 to display a d20 icon. If empty, no icon will be displayed.',
        scope: 'world',
        config: true,
        default: '',
        type: String
    });

    // Register system settings
    game.settings.register('modular-foundry-system', 'showHiddenRoll', {
        name: 'Show hidden rolls',
        hint: 'This setting will show the hidden rolls to GMs for verification purposes.',
        scope: 'world',
        config: true,
        default: '',
        type: Boolean
    });

    game.settings.register('modular-foundry-system', 'hide0.0.9BreakingMessage', {
        name: 'Hide 0.0.9 breaking change message',
        hint: 'The message will no longer be displayed on reload.',
        scope: 'world',
        config: true,
        default: false,
        type: Boolean
    });

    // Register sheet application classes
    Actors.unregisterSheet('core', ActorSheet);
    Actors.registerSheet('modular-foundry-system', CharacterSheet, {
        makeDefault: true,
        types: ['character'],
        label: 'Default'
    });
    Actors.registerSheet('modular-foundry-system', TemplateSheet, {
        makeDefault: true,
        types: ['_template'],
        label: 'Default'
    });

    setInitiativeFormula();

    return true;
});

/**
 * Sets initiative formula for all tokens
 */
function setInitiativeFormula() {
    Combatant.prototype._getInitiativeFormula = function() {
        let initF = game.settings.get('modular-foundry-system', 'initFormula');
        let formula = initF || '1d20';

        CONFIG.Combat.initiative.formula = formula;

        console.debug('Initiative formula : ' + formula);

        return CONFIG.Combat.initiative.formula || game.system.data.initiative;
    };

    Combatant.prototype.getInitiativeRoll = async function(rawFormula) {
        return new Formula(rawFormula || this._getInitiativeFormula());
    };

    Combatant.prototype.rollInitiative = async function(rawFormula) {
        let formula = await this.getInitiativeRoll(rawFormula);

        await formula.compute(this.actor.data.data.props, { defaultValue: '0' });
        return this.update({ initiative: formula.result });
    };

    Combat.prototype.rollInitiative = async function(ids, {
        rawFormula = null,
        updateTurn = true,
        messageOptions = {}
    } = {}) {
        // Structure input data
        ids = typeof ids === 'string' ? [ids] : ids;
        const currentId = this.combatant?.id;
        const rollMode = messageOptions.rollMode || game.settings.get('core', 'rollMode');

        // Iterate over Combatants, performing an initiative roll for each
        const updates = [];
        const messages = [];
        for (let [i, id] of ids.entries()) {

            // Get Combatant data (non-strictly)
            const combatant = this.combatants.get(id);
            if (!combatant?.isOwner) return results;

            // Produce an initiative roll for the Combatant
            const formula = await combatant.getInitiativeRoll(rawFormula);
            let phrase = new ComputablePhrase('${' + formula.raw + '}$');
            await phrase.compute(combatant.actor.data.data.props, { defaultValue: '0' });
            updates.push({ _id: id, initiative: phrase.result });

            // Construct chat message data
            let messageData = foundry.utils.mergeObject({
                speaker: ChatMessage.getSpeaker({
                    actor: combatant.actor,
                    token: combatant.token,
                    alias: combatant.name
                }),
                flavor: game.i18n.format('COMBAT.RollsInitiative', { name: combatant.name }),
                flags: { 'core.initiativeRoll': true }
            }, messageOptions);


            const chatData = await postAugmentedChatMessage(phrase, messageData, {
                create: false,
                rollMode: combatant.hidden && (['roll', 'publicroll'].includes(rollMode)) ? 'gmroll' : rollMode
            });

            // Play 1 sound for the whole rolled set
            if (i > 0) chatData.sound = null;
            messages.push(chatData);
        }
        if (!updates.length) return this;

        // Update multiple combatants
        await this.updateEmbeddedDocuments('Combatant', updates);

        // Ensure the turn order remains with the same combatant
        if (updateTurn && currentId) {
            await this.update({ turn: this.turns.findIndex(t => t.id === currentId) });
        }

        // Create multiple chat messages
        await ChatMessage.implementation.create(messages);
        return this;
    };
}

/* -------------------------------------------- */
/*  Ready Hook                                  */
/* -------------------------------------------- */

Hooks.once('ready', async function() {
    // Inject custom stylesheet if provided in settings
    if (game.settings.get('modular-foundry-system', 'customStyle') !== '') {
        const link = document.createElement('link');
        link.type = 'text/css';
        link.rel = 'stylesheet';
        link.href = game.settings.get('modular-foundry-system', 'customStyle');
        await document.getElementsByTagName('head')[0].appendChild(link);
    }

    if (game.user.isGM && !game.settings.get('modular-foundry-system', 'hide0.0.9BreakingMessage')) {
        Dialog.prompt({
            title: 'Modular Foundry System - Breaking changes',
            content: '<h1>Warning : Breaking changes in Modular Foundry System v0.0.9</h1>\n' +
                '<p>The dynamic table referencing system changed :&nbsp;</p>\n' +
                '<ul>\n' +
                '<li>To reference another column in the same row in formulas, please use <code>$</code> instead of <code>@</code></li>\n' +
                '<li>To use that reference to target another property, please use <code>@$</code> instead of <code>@@</code></li>\n' +
                '</ul>\n' +
                '<p>If you have sheets that used <code>@</code> in their formulas, they are likely to break and refuse to open. You can fix the template actor, and then use the new Reload Template option in the Context Menu of the bugged actor to reload its template and allow it to work again.</p>\n' +
                '<p>&nbsp;</p>\n' +
                '<p>This message will be displayed until the next version is released. You can disable it in the system settings</p>\n' +
                '<p><input type="checkbox" id="hide0-0-9BreakingMessage" name="hide0-0-9BreakingMessage" /><label for="hide0.0.9BreakingMessage">Do not show again</label></p>\n' +
                '<p>Sorry for the inconvenience, and thank you for using the system !</p>',
            label: 'Close',
            callback: () => {
            },
            render: (html) => {
                let hideCheckbox = $(html).find('#hide0-0-9BreakingMessage');
                hideCheckbox.change((ev) => {
                    let target = $(ev.currentTarget);
                    game.settings.set('modular-foundry-system', 'hide0.0.9BreakingMessage', target.is(':checked'));
                });
            }
        });
    }

    if (game.user.isGM) {
        Dialog.prompt({
            title: 'Modular Foundry System - NAME CHANGE',
            content: '<h1>Modular Foundry System changes name and package !</h1>\n' +
                '                <p>The system is now named <strong>Custom System Builder</strong>, and is available under that name in the system manager.</p>\n' +
                '        <p>To update your worlds, please follow these steps :</p>\n' +
                '        <ol>\n' +
                '            <li><strong>BACKUP YOUR WORLDS</strong>, by copying the worlds directory of your FoundryVTT Data Directory</li>\n' +
                '            <li>Download Custom System Builder v1.0.0 using the manifest at this URL : <a href="https://gitlab.com/linkedfluuuush/custom-system-builder/-/raw/1.0.0/system.json">https://gitlab.com/linkedfluuuush/custom-system-builder/-/raw/1.0.0/system.json</a></li>\n' +
                '            <li>In your worlds directory, locate your world. Edit the <code>world.json</code> file, and set <code>system</code> to <code>custom-system-builder</code>, and <code>systemVersion</code> to <code>1.0.0</code></li>\n' +
                '            <li>Restart the FoundryVTT server to update your world settings</li>\n' +
                '            <li>Launch your world as GM</li>\n' +
                '            <li>Done ! Everything should behave normally !</li>\n' +
                '        </ol>\n' +
                '        <p>Sorry for the inconvenience, and thank you for using the system !</p>',
            label: 'Close',
            callback: () => {
            }
        });
    }

});

// Prepare export buttons
Hooks.on('renderSidebarTab', createExportButtons);

/**
 * Create export button
 * @param sidebar
 * @param jq
 */
function createExportButtons(sidebar, jq) {
    if (sidebar._element[0].id !== 'settings') return;

    if (!game.user.isGM) return;

    /* -------------------------------------------- */
    /*  Export button                               */
    /* -------------------------------------------- */
    let exportButton = document.createElement('button');
    exportButton.innerHTML =
        '<i class="fas fa-download"></i>Export templates JSON';

    exportButton.addEventListener('click', (event) => {
        // Fetch every _template actor
        let templates = game.actors.filter(
            (actor) => actor.data.type === '_template'
        );

        // Cleanup data
        templates = templates.map((tpl) => {
            return {
                _id: tpl.id,
                name: tpl.name,
                data: tpl.data.data
            };
        });

        // Download data as JSON
        saveDataToFile(JSON.stringify(templates), 'text/json', 'export.json');
    });

    /* -------------------------------------------- */
    /*  Import button                               */
    /* -------------------------------------------- */
    let importButton = document.createElement('BUTTON');
    importButton.innerHTML =
        '<i class="fas fa-upload"></i>Import templates JSON';

    importButton.addEventListener('click', (event) => {
        // Create File Picker to get export JSON
        let fp = new FilePicker({
            callback: async (filePath) => {
                // Get file from server
                const response = await fetch(filePath);
                const importedPack = await response.json();

                // Fetch every _template actor
                let templates = game.actors.filter(
                    (actor) => actor.data.type === '_template'
                );

                for (let imported of importedPack) {
                    // If a same name template exist, we replace its data with the imported data
                    let matchingLocalTemplates = templates.filter(
                        (tpl) => tpl.name === imported.name
                    );
                    if (matchingLocalTemplates.length > 0) {
                        for (let match of matchingLocalTemplates) {
                            match
                                .update({
                                    data: {
                                        hidden: imported.data.hidden,
                                        header: imported.data.header,
                                        tabs: imported.data.tabs
                                    }
                                })
                                .then(() => {
                                    match.render(false);
                                });
                        }
                    } else {
                        // If no same name template exists, we create the template from imported data
                        await Actor.create({
                            name: imported.name,
                            type: '_template',
                            data: imported.data
                        });
                    }
                }
            }
        });

        // Tweak to allow only json files to be uploaded / selected
        fp.extensions = ['.json'];
        fp.browse();
    });

    // Add everything cleanly into menu
    let exportTitle = document.createElement('h2');
    exportTitle.innerText = 'Modular Foundry System';

    let exportDiv = document.createElement('div');
    exportDiv.id = 'settings-mfs-export';

    exportDiv.appendChild(exportButton);
    exportDiv.appendChild(importButton);

    let jSidebar = $(sidebar._element[0]);
    let helpBox = jSidebar.find('#settings-documentation');

    helpBox.prev('h2').before(exportTitle);
    helpBox.prev('h2').before(exportDiv);
}

Hooks.on('getActorDirectoryEntryContext', addReloadToActorContext);

function addReloadToActorContext(html, menuItems) {
    menuItems.push({
        callback: li => {
            let id = $(li).data('document-id');
            let actor = game.actors.get(id);

            actor.sheet.reloadTemplate();
        },
        condition: li => {
            let id = $(li).data('document-id');
            let actor = game.actors.get(id);

            return actor.data.type === 'character' && game.user.isGM;
        },
        icon: '<i class="fas fa-sync"></i>',
        name: 'Reload template'
    });
}