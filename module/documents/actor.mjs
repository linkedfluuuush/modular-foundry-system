import { UncomputableError } from '../errors/errors.mjs';
import ComputablePhrase from '../formulas/computablePhrase.mjs';

/**
 * Extend the base Actor document by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class ModularActor extends Actor {
    /** @override */
    prepareData() {
        // Prepare data for the actor. Calling the super version of this executes
        // the following, in order: data reset (to clear active effects),
        // prepareBaseData(), prepareEmbeddedDocuments() (including active effects),
        // prepareDerivedData().
        super.prepareData();
    }

    /**
     * @override
     * Augment the basic actor data with additional dynamic data. Typically,
     * you'll want to handle most of your calculated/derived data in this step.
     * Data calculated in this step should generally not exist in template.json
     * (such as ability modifiers rather than ability scores) and should be
     * available both inside and outside of character sheets (such as if an actor
     * is queried and has a roll executed directly from it).
     */
    prepareDerivedData() {
        const actorData = this.data;

        // Computing values twice because sometimes some props have an initial value of 0
        // and derived values are not correctly calculated
        this._prepareCharacterData(actorData);
    }

    /**
     * Prepare Character type specific data
     */
    _prepareCharacterData(actorData) {
        if (actorData.type !== 'character') return;

        // Make modifications to data here.
        const data = actorData.data;

        // Computing all properties
        let computableProps = {};

        // Computable properties are labels within tabs / header and hidden attributes
        let headerProps = this.fetchAllComputableProps(
            data.header,
            computableProps
        );

        computableProps = {
            ...computableProps,
            ...headerProps
        };

        for (let tab of data.tabs) {
            let tabProps = this.fetchAllComputableProps(tab, computableProps);
            computableProps = {
                ...computableProps,
                ...tabProps
            };
        }

        for (let hidden of data.hidden) {
            computableProps[hidden.name] = hidden.value;
        }

        for (let prop in computableProps) {
            delete data.props[prop];
        }

            let computedProps;
        let uncomputedProps = { ...computableProps };

        // Loop while all props are not computed
        // Some computed properties are used in other computed properties, so we need to make several passes to compute them all
        do {
            computedProps = {};

            // For each uncomputed property, we try compute it
            for (let prop in uncomputedProps) {
                try {
                    computedProps[prop] = (ComputablePhrase.computeMessageStatic(
                        uncomputedProps[prop],
                        data.props
                    )).result;
                    // If successful, the property is added to computedProp and deleted from uncomputedProps
                    delete uncomputedProps[prop];
                } catch (err) {
                    if (err instanceof UncomputableError) {
                        console.debug(
                            'Passing prop ' +
                            prop +
                            ' (' +
                            uncomputedProps[prop] +
                            ') to next round of computation...'
                        );
                    } else {
                        throw err;
                    }
                }
            }

            console.log({
                message: 'Computed props...',
                description: computedProps
            });

            // We add the props computed in this loop to the actor's data
            data.props = {
                ...data.props,
                ...computedProps
            };
        } while (
            // If no uncomputed props are left, we computed everything and we can stop
            // If computedProps is empty, that means nothing was computed in this loop, and there is an error in the property definitions
            // Probably a wrongly defined formula, or a loop in property definition
        Object.keys(uncomputedProps).length > 0 &&
        Object.keys(computedProps).length > 0
            );

        // We log the remaining uncomputable properties for debug
        if (Object.keys(uncomputedProps).length > 0) {
            console.warn('Some props were not computed.');
            console.warn(uncomputedProps);
        }
    }

    /**
     * Gets all computable props in a given component, and returns them and their formulas
     * @param component
     * @param computablePropList
     * @returns {{[p: string]: *}}
     */
    fetchAllComputableProps(component, computablePropList) {
        if (component) {
            // Handling the table case, where the contents list is an Array of Arrays
            if (Array.isArray(component)) {
                for (let subComp of component) {
                    let newPropList = this.fetchAllComputableProps(
                        subComp,
                        computablePropList
                    );
                    computablePropList = {
                        ...computablePropList,
                        ...newPropList
                    };
                }
            } else {
                // Component needs key and value to be computable
                if (component.key && component.value) {
                    computablePropList[component.key] = component.value;
                }

                // Recurse on contents
                if (component.contents) {
                    let newPropList = this.fetchAllComputableProps(
                        component.contents,
                        computablePropList
                    );
                    computablePropList = {
                        ...computablePropList,
                        ...newPropList
                    };
                }
            }
        }

        return computablePropList;
    }
}
