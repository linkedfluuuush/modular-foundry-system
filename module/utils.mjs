/**
 * Encodes string to HTML entities
 * @param text
 * @returns {*|string}
 * @private
 */
export const encodeHTMLEntities = (text) => {
    return text
        ? text.replace(/'/g, function() {
            return '\\\'';
        })
        : '';
};

/**
 * Posts a chat message with a computed phrase data
 * @param {{buildPhrase: string, values: {}}} textContent
 * @param msgOptions
 * @param rollMode
 * @param create
 * @returns {Promise<void>}
 */
export const postAugmentedChatMessage = async (textContent, msgOptions = {}, { rollMode, create = true } = {}) => {
    rollMode = rollMode || game.settings.get('core', 'rollMode');

    // chat-roll is just the html template for computed formulas
    const template_file = `systems/modular-foundry-system/templates/chat/chat-roll.html`;

    let phrase = textContent.buildPhrase;
    let values = textContent.values;

    let rolls = [];

    // Render all formulas HTMLs
    for (let key in values) {
        phrase = phrase.replace(key, await renderTemplate(template_file, {
            rollData: values[key],
            jsonRollData: JSON.stringify(values[key]),
            rollMode: rollMode
        }));

        for (let roll of values[key].rolls) {
            rolls.push(Roll.fromData(roll.roll));
        }
    }

    let msgRoll = null;
    let chatRollData = {};
    if (rolls.length > 0) {
        const pool = PoolTerm.fromRolls(rolls);
        msgRoll = Roll.fromTerms([pool]);

        chatRollData = {
            roll: msgRoll,
            type: CONST.CHAT_MESSAGE_TYPES.ROLL,
            rollMode
        };
    }

    let whisper = null;

    // If setting expandRollVisibility is checked, we apply the appropriate whispers to the message
    if (
        game.settings.get(
            'modular-foundry-system',
            'expandRollVisibility'
        )
    ) {
        let gmList = game.users
            .filter((user) => user.isGM)
            .map((user) => user.id);

        switch (rollMode) {
            case CONST.DICE_ROLL_MODES.PRIVATE:
                whisper = gmList;
                break;
            case CONST.DICE_ROLL_MODES.BLIND:
                whisper = gmList;
                break;
            case CONST.DICE_ROLL_MODES.SELF:
                whisper = [game.userId];
                break;
            default:
                break;
        }
    }

    let msg = new ChatMessage(foundry.utils.mergeObject(
        msgOptions,
        foundry.utils.mergeObject({
                content: phrase,
                whisper,
                sound: CONFIG.sounds.dice
            },
            chatRollData
        )
    ));

    if (create) {
        // Final chat message creation
        return ChatMessage.create(foundry.utils.mergeObject(
            msgOptions,
            foundry.utils.mergeObject({
                    content: phrase,
                    whisper,
                    sound: CONFIG.sounds.dice
                },
                chatRollData
            )
        ));
    } else {
        if (rollMode) {
            msg.applyRollMode(rollMode);
        }
        return msg.data.toObject();
    }
};