class GenericError extends Error {
    constructor(message) {
        super(message);
    }
}

export class UncomputableError extends GenericError {
    constructor(message, field, formula, props) {
        super(message);

        this.field = field;
        this.formula = formula;
        this.props = props;
    }
}
