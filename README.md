# DISCLAIMER - This project is discontinued, as it changed name. It is now available at https://gitlab.com/linkedfluuuush/custom-system-builder


# Modular System

Build you custom sheets without any line of code !

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/Z8Z6AJTSY)

## Introduction

This system is intended to easily build sheets in FoundryVTT with dynamic display and roll capabilities, in a game
agnostic way.

If you have any issue, request or question, please report
it [here](https://gitlab.com/linkedfluuuush/modular-foundry-system/-/issues/new). You can also ask on
the [FoundryVTT Discord](https://discord.gg/foundryvtt), I am @LinkedFluuuush there !

If you want to contribute to the project, please see [CONTRIBUTING.md](https://gitlab.com/linkedfluuuush/modular-foundry-system/CONTRIBUTING.md).

If you like this system and want to support me, you can [buy me a coffee](https://ko-fi.com/linkedfluuuush) !

## Incompatibilities

- This system tinkers with initiative rolls and calculations, it might cause problems with modules modifying initiative
  as well.

## Index

- [1. Before we start](#1-before-we-start)
- [2. Creating a basic sheet](#2-creating-a-basic-sheet)
    - [2.1. Adding, editing and deleting tabs](#21-adding-editing-and-deleting-tabs)
    - [2.2. Adding components to tabs and header](#22-adding-components-to-tabs-and-header)
    - [2.3. Creating a character](#23-creating-a-character)
- [3. Component library](#3-component-library)
    - [3.1. Label](#31-label)
    - [3.2. Text fields](#32-text-fields)
    - [3.3. Rich text area](#33-rich-text-area)
    - [3.4. Checkbox](#34-checkbox)
    - [3.5. Drop-down list](#35-drop-down-list)
    - [3.6. Panel](#36-panel)
    - [3.7. Table](#37-table)
    - [3.8. Dynamic table](#38-dynamic-table)
- [4. Formulas](#4-formulas)
    - [4.1. Plain formulas](#41-plain-formulas)
    - [4.2. Conditions](#42-conditions)
    - [4.3. Use text !](#43-use-text-)
    - [4.4. Add rolls](#44-add-rolls)
    - [4.5. Reuse formula results](#45-reuse-formula-results)
    - [4.6. User inputs](#46-user-inputs)
    - [4.7. Hide formulas in chat messages](#47-hide-formulas-in-chat-messages)
- [5. And more !](#5-and-more-)
    - [5.1. Use your own CSS](#51-use-your-own-css)
    - [5.2. Initiative formula](#52-initiative-formula)
    - [5.3. Roll visibility](#53-roll-visibility)
    - [5.4. Roll Icons](#54-roll-icons)
    - [5.5. Export your templates](#55-export-your-templates)
- [6. Special Thanks](#6-special-thanks)

## 1. Before we start

This system defines two Actor types : Templates (`_template`) and Characters (`character`). The `_template` type is for
sheet-building, and the `character` type uses a template to represent a Character. Templates can not be filled out, and
are not supposed to be. Their sole purpose is to build and edit sheet structures, in an easy-to-use way. Character
sheets are where the magic happens.

Sheets are divided in 3 main parts :

- The header, located between the actor's name and the tab selector
- The tab selector, delimited by two horizontal lines
- The tab section, below the tab selector, showing the current selected tab structure

![Sheet sections](docs/imgs/sheet-sections.png)

The header part and the tab section both act in the same way, which is why they will be covered by the same section of
this doc.

The term `key` is used many times in this doc. It refers to an identifier of fields or tabs. It must always be a string
composed of letters (upper and lowercase), numbers and underscores only. Keys must be unique throughout the sheet.

## 2. Creating a basic sheet

To create a basic sheet, you will want to create a `_template` type actor. Template actors name should be unique, as
they will be used to link a template to a Character later on. Once your `_template` is created, its sheet opens, looking
like this.

![Blank _template sheet](docs/imgs/blank_template.png)

### 2.1. Adding, editing and deleting tabs

To add a tab to your sheet, click the `+` icon in the tab selector section. This opens a pop-up dialog to let you enter
the tab details.

![Add new tab](docs/imgs/new_tab.png)

You must enter the tab name, which can be anything, and the tab key.

You can edit the currently selected tab name and key with the edit button on the right, as well as delete it.
Additionally, you can reorder your tabs using the arrows on the left and right of the tab name.

![Tab controls](docs/imgs/tab-controls.png)

### 2.2. Adding components to tabs and header

To add a component to the sheet structure, click the `+` icon in the section you want to add the component in. It will
open a pop-up dialog asking the details of your new component.

![Add new component](docs/imgs/new-component.png)

You must first choose your component's type, and you can add a key. The key will be used for various purposes, including
data-saving for the characters and HTML class added to the components in the resulting sheets.

See [here](#51-use-your-own-css) for more information on CSS enhancing.

The components' documentation is [here](#3-component-library).

Once your component has been added to the sheet, you can click on it to edit its configuration.

![Edit component](docs/imgs/edit-component.png)

New buttons are available at the bottom of the pop-up dialog, allowing you to delete the component, or move it inside
its container.

### 2.3. Creating a character

When your template is complete, you can associate it to a character. Create an actor with type character, and its sheet
will open, looking like this.

![Blank character sheet](docs/imgs/blank-character.png).

On the top right of the sheet, a drop-down list allows you to select the template to use, and a button which applies the
template. If you modify your template, you will need to click this button again to update the character sheet. This
prevents any mistake deleting fields from the character sheets. This action is available only to GMs and Assistants.

The reload action is also available in the context menu on the characters list, though it just reloads the current
template and does not allow for template changing.

## 3. Component library

**REMINDER :** The key is often optional, but MUST be unique through ALL the sheet to work properly if set. The key must
be composed of letters (upper and lowercase), numbers, dashes and underscores only.

You can add CSS classes on every component, which will be added to the rendered component on the sheet.
See [here](#51-use-your-own-css) for more information on CSS enhancing.

### 3.1. Label

Labels are simple text rendered in the sheet. They can be dynamic and can trigger a chat message.

![Label configuration dialog](docs/imgs/new-label.png)

- Key : optional. If set, the label text can be re-used in [formulas](#4-formulas) by referring to the key.
- Label style : You can choose the style of the label between title, subtitle, bold text and default. This will only
  affect the label's rendering.
- Size : Specifies the size of the field. 4 options are available :
    - Auto : The field will take all the available space, resizing automatically.
    - Small / Medium / Large : The field will have a fixed size, depending on your choice.
- Label text : The label's displayed text. Can use [formulas](#4-formulas).
- Label icon : You can add an icon in front of the label. Use a valid [fontawesome](https://fontawesome.com) icon name,
  like `dice-d20`.
- Label roll message : If set, will post this text into a Chat Message on a click on the Label. Can
  use [formulas](#4-formulas).

### 3.2. Text fields

Text fields are one-lined fields to input any string you want.

![Text field configuration dialog](docs/imgs/new-text-field.png)

- Key : mandatory - will not save otherwise
- Label : If set, will display this label next to the field on the sheet
- Size : Specifies the size of the field. 4 options are available :
    - Auto : The field will take all the available space, resizing automatically.
    - Small / Medium / Large : The field will have a fixed size, depending on your choice.
- Format : Enforces a format on the user input. Currently there are two format options :
    - Integer : Accepts only integers, positive or negative
    - Numeric : Accepts decimal numeric values, positive or negative
- Maximum length : sets the maximum number of characters the text field can accept.
- Default value : If set, the field will have this value by default

### 3.3. Rich text area

Rich text areas are fields which can contain enriched HTML data. They come with a WYSIWYG editor.

![Rich text area configuration dialog](docs/imgs/new-rta.png)

- Key : mandatory - will not save otherwise
- Label : If set, will display this label next to the field on the sheet
- Default value : If set, the field will have this value by default
- Style : Three styles are available
    - In sheet editor : Rich text will be displayed on the sheet, with a button to open the editor directly within the
      sheet
    - Dialog editor : Rich text will be displayed on the sheet, with a button to open a pop-up dialog containing the
      editor
    - Icon only : Only an icon to open a pop-up dialog containing the editor will be displayed on the sheet, for tight
      spaces

### 3.4. Checkbox

Checkboxes are checkboxes. Sometimes it's that simple.

![Checkbox configuration dialog](docs/imgs/new-checkbox.png)

- Key : mandatory - will not save otherwise
- Label : If set, will display this label next to the checkbox on the sheet
- Size : Specifies the size of the field. 4 options are available :
    - Auto : The field will take all the available space, resizing automatically.
    - Small / Medium / Large : The field will have a fixed size, depending on your choice.

### 3.5. Drop-down list

Drop-down lists are fields with limited options.

![Drop down configuration dialog](docs/imgs/new-select.png)

- Key : mandatory - will not save otherwise
- Label : If set, will display this label next to the field on the sheet
- Options : The value list displayed in the sheet. Must be entered in the format `key - value`, with `key` an
  alphanumeric lowercase string, and `value` anything.
- Default value : Optional. If set, the drop-down list will select the `key` entered as default value. If not set, the
  drop-down list will include an empty option at the beginning of the list.

### 3.6. Panel

Panels are invisible containers designed to display other components in a specific layout.

![Panel configuration dialog](docs/imgs/new-panel.png)

- Key : optional
- Panel layout : Sets the future components layout in the panel
    - Vertical : Components will display vertically, one after the other, taking all the available width.
    - Horizontal : Components will display horizontally, from left to right.
    - Grid : Components will display horizontally, in lines of n components.
- Panel alignment : Sets the alignment of the components
    - Center :  Will center all components in their virtual column
    - Left : Will place components on the left, leaving a space on the right of the panel
    - Right : Will place components on the right, leaving a space on the left of the panel
    - Justify : Will center components in their columns, but will not leave space in the left and right of the panel.

Once created, the panel will look like this in the template editor :

![Panel editor](docs/imgs/panel-editor.png)

The `+` button is used to add new components to the panel, in the same way as the tabs and header. The panel's header
indicates the panel's key for reference, and will not be shown in the definitive sheets, nor will be the outline. The
panel's header is used to edit the panel's settings.

### 3.7. Table

Tables are strict containers with specified column and row counts.

![Table configuration dialog](docs/imgs/new-table.png)

- Key : optional
- Row count : How many rows must the table have ?
- Column count : How many columns must the table have ?
- Table layout : This is a code to specify the columns alignment. Each column has 1 letter in the code, which indicates
  if the column should be aligned to the left (`l`), to the right (`r`) or centered (`c`).

Once created, the table will look like this in the template editor :

![Table editor](docs/imgs/table-editor.png)

The `+` button are used to add new components into the table's cells, in the same way as the tabs and header. The
table's header indicates the table's key for reference, and will not be shown in the definitive sheets, nor will be the
outline. The table's header is used to edit the table's settings.

### 3.8. Dynamic table

Dynamic tables are a component allowing players to make custom lists in their sheets with a layout specification.

![Dynamic table configuration dialog](docs/imgs/new-dtable.png)

- Key : mandatory - will not save otherwise
- Make column name bold : If set, column names will be bold on the table, to emphasize the head of the table.

Once created, the dynamic table will look like this in the template editor :

![Dynamic table editor](docs/imgs/dynamic-table-editor.png)

The `+` button is used to create new columns to the dynamic table. The dialog that opens is the same as panels, tabs and
headers components, with two additional configuration settings :

![Dynamic table - Add new column](docs/imgs/dynamic-table-new-component.png)

- Alignment : How the column should be aligned in the dynamic table
- Column name : If set, will be displayed as the column's name

## 4. Formulas

In Label text or Roll Messages, you can use Formulas to enhance your sheets.

### 4.1. Plain formulas

To mark a formula, inside your text, use the delimiters `${` and `}$`. Inside thoses delimiters, you can use the keys
you defined for any label, text field, rich text area or checkbox. You can also use mathematical operators, as well as
common mathematical functions, such as `floor()`, `ceil()` or `abs()`. Formulas are parsed
with [the math.js library](https://mathjs.org), so you can use any expression valid in this library. Here
is [a summary of the available functions](https://mathjs.org/docs/reference/functions.html).

For example, the modifier formula for Strength in DnD can be expressed as `${floor((STR - 10) / 2)}$`, assuming the
Strength input was given the key `STR`.

To reference a property in a dynamic table, you can use `$` followed by the column key. This will be computed to be the
value of the field in the same row as the component holding the formula. For example, if you have a `damage` text field
in you dynamic table, you can use it in a label of the same row like so : `The weapon deals ${$damage}$ wounds !`

You can use aggregation functions referencing the dynamic table's fields, outside the dynamic table, by referencing them
like `dynamicTableKey$componentKey`. For instance, to sum the encumberance of a characters `possession` table,
use `sum(possession$enc)`.

You can also reference a field via another field ! If you prefix the key by an `@`, the value will be computed as the
property with the name equal to the value of the field. For example,with a set of attributes properties already
defined `STR`, `CON`, `DEX`, `WIS`, you could define a Drop-down field keyed `attr` for selecting one attribute. In that
case, if `STR` is selected in the drop-down, `@attr` will compute to the value of the `STR` property.

### 4.2. Conditions

You can add conditions to your formulas using the ternary operator : `<condition> ? <value if true> : <value if false>`.

The condition part is evaluated as a boolean :

- You can use comparison operators like
    - `==` for equality
    - `>` and `>=`, respectively 'greater than' and 'greater than or equals'
    - `<` and `<=`, respectively 'lower than' and 'lower than or equals'
    - `!=` for 'not equals'
- Any number higher than 0 will be considered true
- You can directly use the key of a checkbox to test it is checked
    - Example : `${ checkboxKey ? 'Checked' : 'Not checked' }$`

> &#x26a0;&#xfe0f; **Some common syntax in other programming languages won't work**: For example, you can not use the 'NOT' `!` operator.

### 4.3. Use text !

You can add text to your formulas to add flavor to your sheets and messages. To use text, you must enclose it in
quotes `'`. If you wish to use a quote in the text, you can prefix it by a backslash  `\`, it will be parsed correctly.
For example : `${'You can\'t use this item !'}$`

You can concatenate predefined text and field values by using the function `concat()`. If you do so, field values must
be enclosed in the `string()` function to work correctly. For
example : `concat('Deal ', string(strMod+dmgMod), ' damage to the target.')`.

### 4.4. Add rolls

You can use rolls in your formulas. To do so, inside the formula markers, use brackets (`[` and `]`) to define your
rolls. In roll expressions, you can also use keys, but you must surround them with colons. Rolls are performed using
FoundryVTT Roll API.

For example, to roll for Shadowrun's Strength, you can define the following formula : `${[:STR:d6cs>=5]}$`.

Roll operations can be defined inside the roll, or outside. In terms of pure result, `${[1d20 + 3]}$`
and `${[1d20] + 3}$` are equivalent, but I would recommend using the latter.

When used in chat messages, formulas are explained by a small box displayed on click.

![Chat example](docs/imgs/chat-example.png)

> &#x26a0;&#xfe0f; **Rolls can be used only in messages, not in label values.**

> &#x2139;&#xfe0f; The roll system is compatible with the Dice so Nice module !

### 4.5. Reuse formula results

In many cases you will want to re-use formula results in a chat message, to display additional information. You can
assign a formula result to a variable using the operator `:=`. You can then reuse the variable as a key later on in the
same message.

For example, using Warhammer's SL system :

```
Rolling ${roll:=[1d100]}$. 
SL = ${SL:=floor(WS/10) - floor(roll/10)}$. 
Damage : ${9 + SL}$
```

### 4.6. User inputs

You can ask for user inputs in formula using the syntax `?{<varName>}`. This will open a pop-up dialog during formula
computing asking for all the inputs in the formula.

The user inputs variables can be reused later on as keys in the same message.

> &#x26a0;&#xfe0f; **User inputs can be used only in messages, not in label values.**

### 4.7. Hide formulas in chat messages

When using formulas in chat-messages, you can hide some formulas to perform preliminary calculations assigned to
reusable variables. To do so, prefix your formula with `#`, for example :

`${# testResult := target - roll}$`

> &#x26a0;&#xfe0f; **Formula hiding can be used only in messages, not in label values.**

## 5. And more !

### 5.1. Use your own CSS

You can customize your sheets with your own CSS. You can add a CSS file in the system configuration. The path must lead
to a CSS file, starting from Foundry's Data Directory.

### 5.2. Initiative formula

In the system settings, you can set your custom initiative formula. This formula must be formatted like a roll formula,
returning only an integer. The `${}$` must not be set around the formula.

### 5.3. Roll visibility

By default, chat messages with rolls will follow FoundryVTT's roll visibility's system, meaning that the messages will
always be visible by everyone but

- When Public Roll is selected, the roll & formulas data will be visibile by everyone.
- When Private GM Roll is selected, the roll & formulas data will be visible by the GM and the message's author, other
  players will see a question mark instead.
- When Blind GM Roll is selected, the roll & formulas data will be visible by the GM only, other players will see a
  question mark instead.
- When Self Roll is selected, the roll & formulas data will be visible by the author only, other players will see a
  question mark instead.

In the system's settings, you can expand the roll visibility to the whole messages posted using the sheets. The message
will be whispered to its recipients only, and not polluting the chat Log. Players will still be able to show the message
to everyone using the right-click > Show to everyone option, and the roll and formulas data will follow the rules above.

### 5.4. Roll Icons

You can add an icon next to rollable labels to make them more visible. To do so, in the system configuration enter a
valid [fontawesome](https://fontawesome.com) icon name, like `dice-d20`, to show this icon next to all rollable items.

### 5.5. Export your templates

You can export your templates using the button in the settings tab, and reimport them in another instance using the
Import button. This will import every template configuration. If the receiving instance has templates with names
existing in the export file, the templates will be overwritten.

## 6. Special Thanks

Thanks to Seregras for the [Sandbox System Builder](https://gitlab.com/rolnl/sandbox-system-builder) which inspired me
very much building this system. I hope I will not cause too much trouble to them, after building a "concurrent" system.

Thanks to the FoundryVTT dev community for their help and reactivity on the Discord !
