# CHANGELOG

## 0.0.10

- Added system name change pop-up

## 0.0.9

- **BREAKING** - Added ability to reference the value of a field in formula outside dynamic tables
  - This feature changed dynamic table referencing system, it now uses a `$` instead of the `@`
- Added more formula explanations on chat messages
- Added context-menu action to reload sheet template
- Disabled sheet reloading for non GM players
- Fixed tab edition bug when tab key was an integer

## 0.0.8

- Fixed an issue with empty string handling in formulas
- Fixed an issue with chat messages without rolls

## 0.0.7

- Added compatibility with the Dice so Nice module
- Fixed issue with actor data calculation leading to errors in sheet display
- Fixed issue with formula calculations and string handling in formulas

## 0.0.6

- Added initiative formula setting
- Added basic text-field formats
- Added user inputs for formulas
- Added field sizing on sheets
- Fixed core version compatibility
- Fixed label HTML display
- Fixed dynamic tables aggregation functions handling
- Fixed minor error with math.js library

## 0.0.5

- Fixed bug on hidden properties calculation
- Fixed rendering of rich text elements in chat messages
- Added possibility to add an icon in front of labels
- Added aggregation capabilities on dynamic table values

## 0.0.4

- Added hidden properties, used for intermediate calculations only
- Added better explanation of rolls & formulas
- Added option to display icon next to rollable labels
- Added CSS class customization on components
- Added edit button float on dialog editors RTF
- Added option to set first line of array 'head'
- Code cleanup - Separate character and _template sheet codes

## 0.0.3

- Fixed naming errors

## 0.0.2

- Corrected typo in repo name
- Updated README.md & TODO.md

## 0.0.1

- System init