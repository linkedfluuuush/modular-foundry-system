// Add attributes button
$(document).on('click', '.mfs-attributes #addAttribute', (ev) => {
    const target = $(ev.currentTarget);

    // Last row contains only the add button
    const lastRow = target.parents('tr');

    // Create new row
    const newRow = $(`<tr class="mfs-hidden-attribute">
        <td><input class="mfs-attribute-name"/></td>
        <td><textarea class="mfs-attribute-formula"></textarea></td>
        <td class="mfs-attribute-controls"><a class="mfs-delete-hidden-attribute"><i class="fas fa-trash"></i></a></td>
    </tr>`);

    // Insert new row before control row
    lastRow.before(newRow);
});

// Delete attribute button
$(document).on(
    'click',
    '.mfs-attributes .mfs-delete-hidden-attribute',
    (ev) => {
        // Get attributes row
        const target = $(ev.currentTarget);
        let row = target.parents('tr');

        // Remove it from the DOM
        $(row).remove();
    }
);
