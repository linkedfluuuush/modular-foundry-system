let mfsComponentEditorPreviousType;

// Change displayed fields based on previous and new types
function changeDisplayedControls(ev) {
    const target = $(ev.currentTarget);
    const newType = target.val();

    // Hide previous type's fields
    $(
        '.mfs-component-editor-dialog .mfs-' +
        mfsComponentEditorPreviousType +
        '-editor'
    ).slideUp(200);

    // Hide new type's fields
    $('.mfs-component-editor-dialog .mfs-' + newType + '-editor').slideDown(
        200
    );
}

function toggleRichTextRollEditor(ev) {
    let checkbox = $(ev.currentTarget);

    tinymce.remove('textarea#labelRollMessage');

    if (checkbox.is(':checked')) {
        tinymce.init({
            ...CONFIG.TinyMCE,
            selector: 'textarea#labelRollMessage'
        });
    }
}

function toggleRichTextLabelEditor(ev) {
    let checkbox = $(ev.currentTarget);

    tinymce.remove('textarea#labelText');

    if (checkbox.is(':checked')) {
        tinymce.init({
            ...CONFIG.TinyMCE,
            selector: 'textarea#labelText'
        });
    }
}

// Each time the type Select is clicked, we save its current type
// Each time it changed, we change the type display
$(document)
    .on('click', '.mfs-component-editor-dialog #compType', (ev) => {
        const target = $(ev.currentTarget);
        mfsComponentEditorPreviousType = target.val();
    })
    .on(
        'change',
        '.mfs-component-editor-dialog #compType',
        changeDisplayedControls
    )
    .on(
        'change',
        '.mfs-component-editor-dialog #rollRichText',
        toggleRichTextRollEditor
    )
    .on(
        'change',
        '.mfs-component-editor-dialog #labelRichText',
        toggleRichTextLabelEditor
    );