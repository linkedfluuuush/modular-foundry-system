// Displays roll explanation in a floating div
async function expandMfsRoll(ev) {
    // Get clicked roll
    const target = ev.currentTarget;
    const data = $(target).data('roll-data');

    data.arrayExplanation = [];

    // Adding roll results to the explanation
    for (let roll of data.rolls) {
        let rollObject = Roll.fromData(roll.roll);
        let htmlTooltip = $(await rollObject.getTooltip());
        let diceIcons = htmlTooltip.find('.dice-rolls li');

        let spanIcons = [];
        for(let icon of diceIcons){
            let span = $('<span>');
            span.addClass($(icon).attr('class'));
            span.addClass('mfs-roll-tooltip-dice');
            span.text($(icon).text());

            spanIcons.push(span.prop('outerHTML'));
        }

        data.arrayExplanation.push({
            name: roll.formula,
            value: roll.roll.total,
            dice: spanIcons.join('')
        });
    }

    // Going through all used props
    for (let token in data.tokens) {
        // Filtering computed texts, plain numbers and self-explanatory tokens
        if (
            data.tokens.hasOwnProperty(token) &&
            !token.startsWith('_computedText_') &&
            isNaN(token) &&
            token !== data.tokens[token]
        ) {
            data.arrayExplanation.push({
                name: token,
                value: data.tokens[token]
            });
        }
    }

    // Get message element
    const message = $(target).parents('.message-content');

    if (data) {
        const template_file = `systems/modular-foundry-system/templates/chat/chat-roll-tooltip.html`;

        // Render explanation template
        renderTemplate(template_file, data).then((html) => {
            // Add last-minute CSS
            const tooltipWrapper = $(html)[0];
            const tooltip = tooltipWrapper.children[0];

            tooltip.style.width = `fit-content`;
            tooltip.style['min-width'] = `280px`;

            // Append the explanation to the message (Adding to DOM)
            message.append($(tooltipWrapper));

            // Set the position
            const pa = target.getBoundingClientRect();
            const pt = tooltip.getBoundingClientRect();
            tooltip.style.left = `${Math.min(
                pa.x,
                window.innerWidth - (pt.width + 3)
            )}px`;
            tooltip.style.top = `${Math.min(
                pa.y + pa.height + 3,
                window.innerHeight - (pt.height + 3)
            )}px`;
            const zi = getComputedStyle(target).zIndex;
            tooltip.style.zIndex = Number.isNumeric(zi) ? zi + 1 : 100;

            // Adding a handler to remove explanation on click anywhere on the page
            $(document).one('click', () => {
                $(tooltipWrapper).remove();
            });
        });
    }
}

/**
 * Hides roll data if needed by current roll mode
 * @param roll The roll element
 */
const hideRollData = (roll) => {
    roll.data('roll-data', null);
    roll.find('span').text('?');
};

$(document).ready(() => {
    // Adding the handler on every roll in the page, now and future
    $(document).on('click', '.mfs-roll', expandMfsRoll);

    // When rendering a chat message, applying roll mode
    Hooks.on('renderChatMessage', (message, elt) => {
        let rolls = $(elt).find('.mfs-roll');
        for(let rollElt of rolls) {
            let roll = $(rollElt);
            let rollMode = roll.data('roll-mode');

            if (
                rollMode === CONST.DICE_ROLL_MODES.PRIVATE &&
                !game.user.isGM &&
                !message.isAuthor
            ) {
                hideRollData(roll);
            } else if (
                rollMode === CONST.DICE_ROLL_MODES.BLIND &&
                !game.user.isGM
            ) {
                hideRollData(roll);
            } else if (
                rollMode === CONST.DICE_ROLL_MODES.SELF &&
                !message.isAuthor
            ) {
                hideRollData(roll);
            }

            if (roll.data('hidden')) {
                roll.addClass('mfs-hidden-roll');

                if (!(game.user.isGM && game.settings.get(
                    'modular-foundry-system',
                    'showHiddenRoll'
                ))
                ) {
                    roll.hide();
                }
            }
        }
    });
});
