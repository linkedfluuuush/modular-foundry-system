# Planned Features

# Ready for 1.0.0

- Better explanation of rolls & formulas
- Code cleanup - Separate character and _template sheet codes
- CSS class customization on components
- Edit button float on dialog editors RTF
- Add option to set first line of array 'head'
- Hidden properties, used for intermediate calculations only
- Add option to display icon next to rollable labels
- Render correctly rich text contents in roll messages
- Add icons to labels
- Add capability to reference dynamic table values by aggregation functions
- Text field formats : add possibility to add a format check on text fields
- Initiative formula

# Pending for 1.0.0


# 1.1.0

- Code cleanup : Refactor tab renderer to make components into JS classes
- Modifier items : Draggable items to modify/set predetermined properties on sheet drop
- Item templates : Capability to create items draggable on character sheets
- Add capability to reference dynamic table values individually
- Text field formats : add possibility to add a custom allowed character list on text fields
